#FROM debian:latest
FROM alpine:latest
MAINTAINER gamerscomplete/dark-gate

#RUN apt-get update && apt-get install nano telnet

COPY artifacts/genesis /CommandControl/
COPY artifacts/cli /CommandControl/modules/cli
#COPY modules/cli/.aliasDB.gob /CommandControl/modules/cli/.aliasDB.gob
COPY startup.sh /CommandControl/startup.sh

CMD /CommandControl/startup.sh
