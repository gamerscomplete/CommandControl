# Command and Control
## Build
```
./build
```
This will build genesis and cli. This also generates a docker image with a tag of current epoch time and "latest"

gitlab.com/gamerscomplete/commandcontrol:{<epoch time>&&latest}
