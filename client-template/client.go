package clientTemplate

import (
	"gitlab.com/gamerscomplete/uuid"

	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
)

const (
	DEFAULT_TIMEOUT = 1
	SERVICE_NAME    = "template"
)

type Client struct {
	conn *csTools.ServerConnObj
	addr uuid.UUID
}

func NewClient(conn *csTools.ServerConnObj, addr uuid.UUID) *Client {
	return &Client{
		conn: conn,
		addr: addr,
	}
}

func (client *Client) TemplateRequest() (response TemplateResponse, err error) {
	err = client.conn.PackSendReplyTimeout(DEFAULT_TIMEOUT, "GET_TEMPLATE_REQUEST", SERVICE_NAME, client.addr, nil, &response)
	return
}
