package tools

import (
	"fmt"
	"os"
	"runtime"
	"time"
)

func EpochTime() int64 {
	return int64(time.Now().Unix())
}

func Log(message string) {
	//fmt.Println(message)
}

func GetCallerFunction() string {

	// we get the callers as uintptrs - but we just need 1
	fpcs := make([]uintptr, 1)

	// skip 3 levels to get to the caller of whoever called Caller()
	n := runtime.Callers(3, fpcs)
	if n == 0 {
		return "n/a" // proper error her would be better
	}

	// get the info of the actual function that's in the pointer
	fun := runtime.FuncForPC(fpcs[0] - 1)
	if fun == nil {
		return "n/a"
	}

	// return its name
	return fun.Name()
}

func SizePretty(size int64) (formatted string) {
	switch {
	case size < 1024:
		formatted = fmt.Sprintf("%d B", size)
	case size > 1024 && size < 1048576:
		formatted = fmt.Sprintf("%d KB", (size / 1024))
	case size > 1048576 && size < 1073741824:
		formatted = fmt.Sprintf("%d MB", (size / 1048576))
	case size > 1073741824 && size < 1099511627776:
		formatted = fmt.Sprintf("%d GB", (size / 1073741824))
	case size > 1099511627776 && size < 1125899906842624:
		formatted = fmt.Sprintf("%d GB", (size / 1099511627776))
	case size > 1125899906842624:
		formatted = fmt.Sprintf("%d GB", (size / 1125899906842624))
	default:
		fmt.Println("Definitely shouldnt be here in size pretty")
	}
	return formatted
}

func FileExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	//    Log(3, "Failed to check directory")
	return false
}
