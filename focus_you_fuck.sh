#!/bin/bash

go fmt genesis/*.go
go fmt plexus/*.go
go fmt modules/cli/*.go
go fmt modules/template/*.go
go fmt clientServerTools/*.go
