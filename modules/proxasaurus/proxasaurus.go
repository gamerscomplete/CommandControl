/*
 * Types of proxies: {dual_listen, direct_connect,
 */

package main

import (
	"fmt"
	"gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	//    "github.com/hashicorp/yamux"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

type proxyConnection struct {
	proxyType              string
	toID                   string
	fromID                 string
	fromPort, toPort       int
	sizeSent, sizeRecieved int
}

var serverConn = clientServerTools.NewServerConn()

func main() {
	if err := serverConn.Init("proxasaurus"); err != nil {
		log(5, "Failed to initialize server conn with error:", err)
		return
	}

	/*
		// Just some boilerplate code from this point
		newMessage := clientServerTools.Message{Command: "PING"}
		returnChannel, err := serverConn.SendServerMessageTwoWay("local", "genesis", newMessage)
		if err != nil {
			fmt.Println("Failed to send message")
			return
		}
		replyMessage := <-returnChannel
		fmt.Println("Got reply. Command:", replyMessage.Command, "Parameters:", replyMessage.Parameters)
	*/

	//fucker kept diein
	for {
		// Keep er alive
		time.Sleep(3000 * time.Millisecond)
	}
}

//TODO: This doesnt do anyhting anymore since the cstools rework and needs to have functions that take a servermessage
func processServerMessage(message clientServerTools.ServerMessage) {
	switch message.Data.Command {
	case "SHUTDOWN":
		log(1, "Shutdown invoked")
		os.Exit(0)
	case "PING":
		log(0, "Recieved ping from: ", message.FromID, ":", message.FromService)
		replyMessage := clientServerTools.Message{Command: "PONG", Parameters: message.ToID.String()}
		serverConn.SendServerMessageReply(message, replyMessage)
	case "START_INGRESS":
		startIngress(message.Data.Parameters)
	case "START_EGRESS":
		startEgress(message.Data.Parameters)
	case "START_PROXY":
		startProxy()
	default:
		fmt.Println("Recieved unknown command from server: ", message.Data.Command, "With parameters:", message.Data.Parameters)
	}
}

// This really needs to be setup for a datapipe back to genesis
func log(level int, message ...interface{}) {
	//	fmt.Print(message, "\n")
}

func assemblePipeline() string {
	//Communicate with genesis to start a pipeline
	return ""
}

func startProxy() {
	log(3, "StartProxy called. Not implemented")
}

func startIngress(params string) {
	//ingress_port:egress_dest_addr:egress_dest_port:egress_host
	paramFields := strings.Fields(params)
	if len(paramFields) != 4 {
		log(2, "Incorrect number of parameters")
	}

	// Validate request
	/*	if paramFields[0] != "dual_listen" && paramFields[0] != "direct_connect" {
		log(2, "Incorrect message type")
		return
	}*/

	// this is ugly, will do something better later
	ingressPort, err := strconv.Atoi(paramFields[0])
	if err != nil {
		log(2, "Failed to convert field to int")
		return
	}
	egressDestPort, err := strconv.Atoi(paramFields[2])
	if err != nil {
		log(2, "Failed to convert field to int")
		return
	}

	if egressDestPort > 65535 {
		log(2, "Egress port invalid:", paramFields[2])
		return
	}
	if ingressPort > 65535 || ingressPort < 1024 {
		log(2, "Igress port invalid:", paramFields[0])
		return
	}

	//light this firecracker

	//start listen socket
	//    listenSock, err := net.Listen("tcp", ":" + paramFields[0])
	_, err = net.Listen("tcp", ":"+paramFields[0])
	if err != nil {
		log(5, "listen error: ", err)
		return
	}

	//Need to notify genesis of the incoming stream and where it should pipe that to
	//    proxyStream, err := serverConn.StreamSession.Open()
	_, err = serverConn.StreamSession.Open()
	if err != nil {
		log(4, "Opening stream for proxy failed", err)
		return
	}

	//request pipeline
}

func startEgress(params string) {
	//Dial remote port
	//
}

//Open Ingress
//Open Egress
