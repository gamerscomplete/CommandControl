// Provides command line to Command Control
package main

import (
	//	"bufio"
	//	"encoding/gob"
	"fmt"
	"gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	//	"gitlab.com/gamerscomplete/CommandControl/tools"
	//	"os"
	"gitlab.com/gamerscomplete/uuid"
	"strconv"
	"strings"
	"sync"
	"time"
)

//// SERVICE MANAGEMENTas

func commandService(input []string) {
	//	command := strings.Fields(input)
	if len(input) == 0 {
		fmt.Print("\n")
		return
	}
	switch input[0] {
	case "start":
		serviceStartStop(input[1:], "start")
	case "stop":
		serviceStartStop(input[1:], "stop")
	case "list":
		serviceList("")
	default:
		serviceHelp()
	}
}

func serviceStartStop(service []string, action string) {
	serviceSplit := strings.Split(service[0], ":")

	if len(serviceSplit) != 2 {
		fmt.Println("Destination service to start failed to parse")
		return
	}

	toID, err := uuid.ParseHex(serviceSplit[0])
	if err != nil {
		log(2, err)
		return
	}

	//This is a bit ugly and hackish but it felt more right than having 2 almost identical functions
	if action == "start" {
		if err := serverConn.StartService(*toID, serviceSplit[1]); err != nil {
			log(2, "Service start failed:", err)
			return
		}
		fmt.Println("Service successfully started")
	} else if action == "stop" {
		if err := serverConn.StopService(*toID, serviceSplit[1]); err != nil {
			log(2, "Service stop failed:", err)
			return
		}
		fmt.Println("Service successfully stopped")
	}
}

func serviceList(filter string) {
	services, err := serverConn.GetClusterServices(filter)
	if err != nil {
		log(2, "Failed to get cluster services:", err)
		return
	}
	fmt.Print("Services running: (", len(services.Services), ")")
	for serviceName, data := range services.Services {
		fmt.Print(serviceName, "-\n")
		for duration, nodeID := range data {
			fmt.Print("\t", nodeID.String(), " Latency: ", duration, "\n")
		}
	}
	fmt.Print("\n")
}

func serviceHelp() {
	fmt.Println("fukoff")
}

func commandMessage(input []string) {
	//Command should look like "message <to-uuid> <service> <command> <parameters>
	if len(input) < 3 {
		log(2, "Not enough parameters provided to commandMessage")
		return
	}

	if input[0] == "2way" {
		//		newMessage := clientServerTools.Message{Command: input[3], Parameters: strings.Join(input[4:], " ")}

		var toID uuid.UUID
		if input[1] == "local" {
			toID = localID
		} else {
			parsed, err := uuid.ParseHex(input[1])
			if err != nil {
				log(2, err, input[1])
				return
			}
			toID = *parsed
		}

		var reply string
		if err := serverConn.PackSendReplyTimeout(5, input[3], input[2], toID, strings.Join(input[4:], " "), &reply); err != nil {
			log(2, err)
			return
		}
		//		fmt.Print("Command: \"", replyMessage.Command, "\"\nParameters: \"", replyMessage.Parameters, "\"\n")
	} else {
		//TODO: Verify destination is valid.
		//TODO: this needs a timeout
		newMessage := clientServerTools.Message{Command: input[2], Parameters: strings.Join(input[3:], " ")}

		var toID uuid.UUID
		if input[0] == "local" {
			toID = localID
		} else {
			parsed, err := uuid.ParseHex(input[0])
			if err != nil {
				log(2, err, input[0])
				return
			}
			toID = *parsed
		}

		if err := serverConn.SendServerMessage(toID, input[1], newMessage); err != nil {
			log(3, "Failed to send server message")
		}
	}
}

//// Options commands
func commandOptions(command []string) {
	if len(command) < 1 {
		optionsHelp()
		return
	}
	switch command[0] {
	case "set":
		//This should take more than a single value. but for now i dont have a use case, will revisit when i find one
		optionSet(command[1], strings.Join(command[2:], " "))
	case "unset":
		optionUnset(command[1])
	case "list", "ls":
		optionsList()
	default:
		optionsHelp()
	}
}

func optionsHelp() {
	fmt.Println("options [set|unset|list/ls]")
}

func optionsList() {
	for key, value := range optionsDB {
		fmt.Print("\"", key, "\" - ", value, "\n")
	}
}

// Perf commands
func commandPerf(command []string) {
	//TODO: Verify inputs better so messages cant be sent to non existant places
	if len(command) == 0 {
		fmt.Println("Performance testing for message sending. parrallel and serial tests")
		fmt.Println("Usage: perf multi <TO-UUID> <TO-SERVICE> <thread count> <messages per thread>")
		fmt.Println("Usage: perf test <TO-UUID> <TO-SERVICE> <message count>")
		fmt.Println("Usage: perf timed <TO-UUID> <TO-SERVICE> <time to run> (Not implemented)")
		return
	}
	switch command[0] {
	case "timed":
		fmt.Println("Not implemented")
	case "multi":
		// perf multi <thread count> <messages per thread>
		var threadCount int
		var messagesPerThread = 10
		switch len(command) {
		case 5:
			threadCount, _ = strconv.Atoi(command[3])
			messagesPerThread, _ = strconv.Atoi(command[4])
		case 4:
			threadCount, _ = strconv.Atoi(command[3])
			messagesPerThread = 10
		case 3:
			//default it all we didnt get anything
			threadCount = 10
			messagesPerThread = 10
		}
		start := time.Now()

		var wg sync.WaitGroup

		toID, err := uuid.ParseHex(command[1])
		if err != nil {
			log(2, err)
			return
		}

		for i := 0; i < threadCount; i++ {
			wg.Add(1)
			go func(destinationID uuid.UUID, destinationService string, messageCount int, threadCount int, wg *sync.WaitGroup) {
				/*
					for ii := 0; ii < messagesPerThread; ii++ {
						var statusMessage = clientServerTools.Message{Command: "PING"}
						returnChannel, err := serverConn.SendServerMessageTwoWay(destinationID, destinationService, statusMessage)
						if err != nil {
							fmt.Println("Failed to send message")
							return
						}
						replyMessage := <-returnChannel

						// Dummy use of variable to consume the channel without causing compile errors.
						// Since perftest is mostly just a hackjob for testing it doesn't really have a use for the response like anything else would
						if replyMessage.Command != "PONG" {
							fmt.Println("Recieved wrong command back: ", replyMessage.Command)
							wg.Done()
							return
						}
						//					if replyMessage.Parameters != destinationID {
						//						fmt.Println("Recieved response from incorrect recipient:", replyMessage.Parameters)
						//						wg.Done()
						//						return
						//					}
					}
				*/
				wg.Done()
			}(*toID, command[2], messagesPerThread, threadCount, &wg)

		}
		wg.Wait()
		elapsed := time.Since(start)
		fmt.Println("Executions:", (threadCount * messagesPerThread), "\nTime:", elapsed)

	case "test":
		/*
			count := 10
			if len(command) > 3 {
				count, _ = strconv.Atoi(command[3])
			}

			fmt.Println("Starting test")
			start := time.Now()

			toID, err := uuid.ParseHex(command[1])
			if err != nil {
				log(2, err)
				return
			}
			for i := 0; i < count; i++ {
				var statusMessage = clientServerTools.Message{Command: "PING"}
				returnChannel, err := serverConn.SendServerMessageTwoWay(*toID, command[2], statusMessage)
				if err != nil {
					fmt.Println("Failed to send message")
					return
				}
				replyMessage := <-returnChannel

				// Dummy use of variable to consume the channel without causing compile errors.
				// Since perftest is mostly just a hackjob for testing it doesn't really have a use for the response like anything else would
				if replyMessage.Command == "PONG" {
				}
			}
			elapsed := time.Since(start)
			fmt.Println("Executions:", count, "\nTime:", elapsed)
		*/
	}
}

func commandDisconnect() {
	serverConn.Conn.Close()
}

// Alias commands
func commandAlias(command []string) {
	if len(command) < 1 {
		fmt.Println("This should display alias help")
		return
	}
	switch command[0] {
	case "list", "ls":
		aliasList()
	case "rm":
		aliasRemove(command[1])
	default:
		toCMD := strings.Join(command[1:], " ")
		aliasAdd(command[0], toCMD)
		fmt.Println("Aliasing", command[0], "to", toCMD)
	}
}

func commandStatus() {
	/*
		if serverConn.Conn != nil {
			fmt.Println("Server connection active")
		} else {
			fmt.Println("Not connected to server")
		}
		fmt.Print("Sent size: ", tools.SizePretty(serverConn.SentSize), "\n")
		fmt.Print("Recieved size: ", tools.SizePretty(serverConn.RecievedSize), "\n")

		newMessage := clientServerTools.Message{Command: "PLEXUS_STATUS"}
		returnChannel, err := serverConn.SendServerMessageTwoWay(localID, "genesis", newMessage)
		if err != nil {
			log(3, "Failed to send message", err)
			return
		}
		replyMessage := <-returnChannel
		if replyMessage.Parameters != "" {
			fmt.Print("Local peers:\n")
			fmt.Print(replyMessage.Parameters, "\n")
		} else {
			fmt.Println("No local peers connected")
		}
	*/
}

func commandHelp() {
	fmt.Println("options\nstatus\nlogtest\nperf\ndisc\nalias\nsvc|service")
}
