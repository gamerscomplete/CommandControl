// Provides command line to Command Control
package main

import (
	"bufio"
	"encoding/gob"
	"fmt"
	"gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	//	"gitlab.com/gamerscomplete/CommandControl/tools"
	"os"
	"strconv"
	"strings"
	//	"sync"
	"time"
	//    "github.com/nsf/termbox-go"
)

const (
	optionsFile     = ".ccCLIoptions.gob"
	aliasDBFile     = ".aliasDB"
	DEFAULT_TIMEOUT = time.Second * time.Duration(1)
)

var logLevel = 0
var prompt = "#"

var serverConn = clientServerTools.NewServerConn()
var aliasDB = make(map[string]string)
var optionsDB = make(map[string]string)
var localID = clientServerTools.GetID()

func main() {
	optionsLoad()

	if value, found := optionsDB["log_level"]; found {
		if integer, err := strconv.Atoi(value); err == nil {
			logLevel = integer
		} else {
			fmt.Println("Failed to convert log level from optionsDB")
		}
	}

	if value, found := optionsDB["prompt"]; found {
		log(0, "Prompt option found. Loading")
		prompt = value
	}

	err := serverConn.Init("cli")
	if err != nil {
		log(5, "Failed to initialize server conn with error:", err)
		return
	}
	log(1, "Server conn successfully established")

	aliasInit()

	/*
	       err := termbox.Init()
	       if err != nil {
	           panic(err)
	       }
	       defer termbox.Close()
	       termbox.SetInputMode(termbox.InputEsc)

	       redraw_all()
	   mainloop:
	       for {
	           switch ev := termbox.PollEvent(); ev.Type {
	           case termbox.EventKey:
	               switch ev.Key {
	               case termbox.KeyEsc:
	                   break mainloop
	               case termbox.KeyArrowLeft, termbox.KeyCtrlB:
	                   edit_box.MoveCursorOneRuneBackward()
	               case termbox.KeyArrowRight, termbox.KeyCtrlF:
	                   edit_box.MoveCursorOneRuneForward()
	               case termbox.KeyBackspace, termbox.KeyBackspace2:
	                   edit_box.DeleteRuneBackward()
	               case termbox.KeyDelete, termbox.KeyCtrlD:
	                   edit_box.DeleteRuneForward()
	               case termbox.KeyTab:
	                   edit_box.InsertRune('\t')
	               case termbox.KeySpace:
	                   edit_box.InsertRune(' ')
	               case termbox.KeyCtrlK:
	                   edit_box.DeleteTheRestOfTheLine()
	               case termbox.KeyHome, termbox.KeyCtrlA:
	                   edit_box.MoveCursorToBeginningOfTheLine()
	               case termbox.KeyEnd, termbox.KeyCtrlE:
	                   edit_box.MoveCursorToEndOfTheLine()
	               default:
	                   if ev.Ch != 0 {
	                       edit_box.InsertRune(ev.Ch)
	                   }
	               }
	           case termbox.EventError:
	               panic(ev.Err)
	           }
	           redraw_all()
	       }

	*/

	// OLD METHOD
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print(prompt, " ")
		text, err := reader.ReadString('\n')
		if err != nil {
			log(4, "Reading input failed.")
			break
		}
		if text != "\n" {
			processInput(text)
		}
	}

}

/*
// Function to be given to the clientServerTools to callback on recieved commands from the server
func processServerMessage(message clientServerTools.ServerMessage) {
	switch message.Data.Command {
	case "ERR":
		//Should adjust this to handle more levels later
		log(2, message.Data.Parameters)
	case "DISC":
		//shutdown
	case "PING":
		log(0, "Recieved ping from: ", message.FromID.String(), ":", message.FromService)
		replyMessage := clientServerTools.Message{Command: "PONG", Parameters: message.ToID.String()}
		serverConn.SendServerMessageReply(message, replyMessage)
	case "PM":
		fmt.Println("PM: ", message.Data.Parameters)
	default:
		fmt.Println("Recieved unknown command from server: ", message.Data.Command, "Job:", message.JobID, "With parameters:", message.Data.Parameters)
	}
}
*/
/*func (message clientServerTools.ServerMessage) reply(message clientServerTools.Message) {
	if err := serverConn.SendServerMessage(message.FromID, message.FromService, message); err != nil {
		log(3, "Failed to send server message")
	}
}*/

func processInput(input string) {
	command := strings.Fields(input)
	if len(command) == 0 {
		fmt.Print("\n")
		return
	}
	value, found := aliasDB[command[0]]
	if found {
		newInput := value + " " + strings.Join(command[1:], " ")
		log(0, "Found alias \"", command[0], "\" Aliasing to: ", newInput)
		command = strings.Fields(newInput)
	}
	log(0, "Processing command:", command)
	switch command[0] {
	case "options":
		commandOptions(command[1:])
	case "echo":
		fmt.Println(command[1:])
	case "quit":
		log(0, "Terminating cli")
		os.Exit(0)
	case "svc", "service":
		commandService(command[1:])
	case "help":
		commandHelp()
	case "alias":
		commandAlias(command[1:])
	case "status", "stats":
		commandStatus()
	case "disc":
		commandDisconnect()
	case "perf":
		commandPerf(command[1:])
	case "message":
		commandMessage(command[1:])
	case "time":
		start := time.Now()
		processInput(strings.Join(command[1:], " "))
		fmt.Println(time.Since(start))
	case "logtest":
		for i := 0; i < 7; i++ {
			log(i, "Test message")
		}
	default:
		fmt.Println("Unknown command: " + command[0])
	}
}

func optionSet(option string, value string) {
	optionsDB[option] = value
	//This needs to be made into a value subscription system but this hack will do for now
	if option == "log_level" {
		if integer, err := strconv.Atoi(value); err == nil {
			logLevel = integer
			fmt.Println("Setting log level to:", integer)
		} else {
			fmt.Println("Failed to convert log level from optionsDB")
		}
	} else if option == "prompt" {
		prompt = value
	}
	optionsWrite()
}

func optionUnset(option string) {
	delete(optionsDB, option)
	optionsWrite()
}

func optionsWrite() {
	dataFile, err := os.Create(optionsFile)
	if err != nil {
		log(3, "Failed to create options file with err: ", err)
		return
	}
	dataEncoder := gob.NewEncoder(dataFile)
	if err := dataEncoder.Encode(optionsDB); err != nil {
		log(3, "Failed to write options file with err: ", err)
	}
	dataFile.Close()
}

func optionsLoad() {
	dataFile, err := os.Open(optionsFile)
	if err != nil {
		//log(2, "Failed to open optionsFile", err)
		return
	}
	defer dataFile.Close()

	dataDecoder := gob.NewDecoder(dataFile)
	err = dataDecoder.Decode(&optionsDB)
	if err != nil {
		log(3, "Failed to initialize optionsDB:", err)
	}
	//	log(0, "Options initialization complete")
}

func aliasInit() {
	dataFile, err := os.Open(aliasDBFile)
	if err != nil {
		log(2, "Failed to open aliasdb", err)
		return
	}
	defer dataFile.Close()

	dataDecoder := gob.NewDecoder(dataFile)
	err = dataDecoder.Decode(&aliasDB)

	if err != nil {
		log(3, "Failed to initialize aliasDB:", err)
	}
	log(0, "Alias initialization complete")
}

func aliasWriteDB() {
	dataFile, err := os.Create(aliasDBFile)
	if err != nil {
		fmt.Println(err)
	}

	dataEncoder := gob.NewEncoder(dataFile)
	dataEncoder.Encode(aliasDB)
	dataFile.Close()
}

func aliasAdd(from string, to string) {
	aliasDB[from] = to
	aliasWriteDB()
}

func aliasRemove(alias string) {
	delete(aliasDB, alias)
}

func aliasList() {
	for key, value := range aliasDB {
		fmt.Print("\"", key, "\" - ", value, "\n")
	}
}

/////////////////////////////////////
// Utilities
/////////////////////////////////////

// Prints a formatted log mesage to the CLI. Basic syslog functionality to screen
func log(level int, message ...interface{}) {
	/*
			*  0 - DEBUG - Something's fucking busted
			*  1 - INFO - Informational
			*  2 - WARN - Should be addressed
			*  3 - ERROR - Some functionality unable to complete its task
			*  4 - CRITICAL - Major application instability
			*  5 - FATAL - Application haulting

		    var testColor = "\033[4;0;35m"
	*/

	if level < logLevel {
		return
	}
	switch level {
	case 0:
		fmt.Print("[", colorText("blue", "DEBUG"), "] ", message, "\n")
	case 1:
		fmt.Print("[", colorText("green", "INFO"), "] ", message, "\n")
	case 2:
		fmt.Print("[", colorText("purple", "WARN"), "] ", message, "\n")
	case 3:
		fmt.Print("[", colorText("red", "ERROR"), "] ", message, "\n")
	case 4:
		fmt.Print("[", colorText("cyan", "CRITICAL"), "] ", message, "\n")
	case 5:
		fmt.Print("[", colorText("rainbow", "FATALITY!"), "] ", message, "\n")
		//		fmt.Print("[", colorText("blink", "FATALITY!"), "] ", message, "\n")
	default:
		fmt.Print("UnknownErrLevel: ", level, "Message: ", message, "\n")
	}
}

func colorText(color string, text string) string {
	var colorMap = make(map[string]string)
	colorMap["gray"] = "\033[1;30m"
	colorMap["blink"] = "\033[5m"
	colorMap["cyan"] = "\033[0;36m"
	colorMap["lightCyan"] = "\033[1;36m"
	colorMap["green"] = "\033[0;42m"
	colorMap["blue"] = "\033[0;46m"
	colorMap["red"] = "\033[0;41m"
	colorMap["purple"] = "\033[0;35m"
	colorMap["black"] = "\033[0;30m"
	colorMap["gray"] = "\033[1;30m"
	colorMap["lightGray"] = "\033[0;37m"
	colorMap["noColor"] = "\033[0m"

	if color == "rainbow" {
		var byteString = []byte(text)
		var newText = ""
		for i := 0; i < len(text); i++ {
			for _, value := range colorMap {
				newText += value + string(byteString[i]) + colorMap["noColor"]
				break
			}
		}
		return newText
	}

	if colorCode, found := colorMap[color]; found {
		return colorCode + text + colorMap["noColor"]
	}
	return text
}

///////////////////////////////////////////////////////////////////
/// TERMBOX FUNCTIONS FOR SCREEN DRAWLING!
///////////////////////////////////////////////////////////////////

/*
func redraw_all() {
    const coldef = termbox.ColorDefault
    termbox.Clear(coldef, coldef)
    w, h := termbox.Size()

    midy := h / 2
    midx := (w - edit_box_width) / 2

    // unicode box drawing chars around the edit box
    termbox.SetCell(midx-1, midy, '│', coldef, coldef)
    termbox.SetCell(midx+edit_box_width, midy, '│', coldef, coldef)
    termbox.SetCell(midx-1, midy-1, '┌', coldef, coldef)
    termbox.SetCell(midx-1, midy+1, '└', coldef, coldef)
    termbox.SetCell(midx+edit_box_width, midy-1, '┐', coldef, coldef)
    termbox.SetCell(midx+edit_box_width, midy+1, '┘', coldef, coldef)
    fill(midx, midy-1, edit_box_width, 1, termbox.Cell{Ch: '─'})
    fill(midx, midy+1, edit_box_width, 1, termbox.Cell{Ch: '─'})

    edit_box.Draw(midx, midy, edit_box_width, 1)
    termbox.SetCursor(midx+edit_box.CursorX(), midy)

    tbprint(midx+6, midy+3, coldef, coldef, "Press ESC to quit")
    termbox.Flush()
}
*/
