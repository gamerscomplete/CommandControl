package main

import (
	"bufio"
	"fmt"
	"gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/CommandControl/tools"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

var serverConn = clientServerTools.ServerConnObj{}

func main() {
	if err := serverConn.Init("perftest", processServerMessage); err != nil {
		fmt.Println("blew up", err")
	}
}

func processInput(input string) {
	command := strings.Fields(input)
	if len(command) == 0 {
		fmt.Print("\n")
		return
	}
	switch command[0] {
	case "perf":
		commandPerf(command[1:])
	default:
		fmt.Println("Unknown command: " + command[0])
	}
}

/////////////////////////////////////
// Command functions
/////////////////////////////////////

func commandPeer(command []string) {
	if len(command) < 2 {
		//display help
	}
	switch command[0] {
	case "test":
		var statusMessage = clientServerTools.Message{Command: "PEERTEST", Parameters: command[1]}
		returnChannel, err := serverConn.SendServerMessageTwoWay(statusMessage)
        if err != nil {
            fmt.Println("Failed to send message")
        	return
        }
        replyMessage := <-returnChannel

        // Dummy use of variable to consume the channel without causing compile errors.
        // Since perftest is mostly just a hackjob for testing it doesn't really have a use for the response like anything else would
        if replyMessage.Command == "ACK" {
        }
	default:
		fmt.Println("da fuq")
	}
}



func commandPerf(command []string) {
	if len(command) == 0 {
		fmt.Println("Performance testing for message sending. parrallel and serial tests")
		fmt.Println("Usage: perf multi <thread count> <messages per thread>")
		fmt.Println("Usage: perf test <message count>")
		return
	}
	switch command[0] {
	case "multi":
		// perf multi <thread count> <messages per thread>
		var threadCount int
		var messagesPerThread = 10
		switch len(command) {
		case 3:
			threadCount, _ = strconv.Atoi(command[1])
			messagesPerThread, _ = strconv.Atoi(command[2])
		case 2:
			threadCount, _ = strconv.Atoi(command[1])
			messagesPerThread = 10
		case 1:
			//default it all we didnt get anything
			threadCount = 10
			messagesPerThread = 10
		}
		start := time.Now()

		var wg sync.WaitGroup

		for i := 0; i < threadCount; i++ {
			wg.Add(1)
			go func(messageCount int, threadCount int, wg *sync.WaitGroup) {
				for ii := 0; ii < messagesPerThread; ii++ {
					var statusMessage = clientServerTools.Message{Command: "PERF"}
					//					fmt.Println("Sending message")
					returnChannel, err := serverConn.SendServerMessageTwoWay(statusMessage)
					if err != nil {
						fmt.Println("Failed to send message")
						return
					}

					//					fmt.Println("Waiting for reply")
					replyMessage := <-returnChannel

					// Dummy use of variable to consume the channel without causing compile errors.
					// Since perftest is mostly just a hackjob for testing it doesn't really have a use for the response like anything else would
					if replyMessage.Command == "ACK" {
					}
				}
				wg.Done()
			}(messagesPerThread, threadCount, &wg)

		}
		wg.Wait()
		elapsed := time.Since(start)
		fmt.Println("Executions:", (threadCount * messagesPerThread), "\nTime:", elapsed)

	case "test":
		count := 10
		if len(command) > 1 {
			count, _ = strconv.Atoi(command[1])
		}

		fmt.Println("Starting test")
		start := time.Now()
		for i := 0; i < count; i++ {
			var statusMessage = clientServerTools.Message{Command: "PERF"}
			returnChannel, err := serverConn.SendServerMessageTwoWay(statusMessage)
			if err != nil {
				fmt.Println("Failed to send message")
				return
			}
			replyMessage := <-returnChannel

			// Dummy use of variable to consume the channel without causing compile errors.
			// Since perftest is mostly just a hackjob for testing it doesn't really have a use for the response like anything else would
			if replyMessage.Command == "ACK" {
			}
		}
		elapsed := time.Since(start)
		fmt.Println("Executions:", count, "\nTime:", elapsed)
	}
}

// Function to be given to the clientServerTools to callback on recieved commands from the server
func processServerMessage(message clientServerTools.Message) {
	switch message.Command {
	case "ERR":
		//Should adjust this to handle more levels later
		logMessage(message.Parameters, 2)
	case "DISC":
		//shutdown
	default:
		fmt.Println("Recieved command from server: ", message.Command, message.Parameters)
	}
}
