package main

import (
	"fmt"
	"os"

	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
)

const (
	LOG_LEVEL    = 0
	SERVICE_NAME = "template"
)

var (
	_serverConn = csTools.NewServerConn()
	localID     = csTools.GetID()
)

func main() {
	_serverConn.RegisterHandler("STUFF", doStuff)

	// Set the name of this module
	if err := _serverConn.Init(SERVICE_NAME); err != nil {
		log(5, "Failed to initialize server conn with error:", err)
		return
	}
	csTools.Wait()
	shutdown()
}

func log(level int, message ...interface{}) {
	if level >= LOG_LEVEL {
		fmt.Print("(", SERVICE_NAME, ")", message, "\n")
	}
}

func shutdown() {
	log(1, "Shutdown called")
	os.Exit(0)
}

func doStuff(message csTools.ServerMessage) {
	//Do things
}
