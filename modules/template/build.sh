#!/bin/bash
SERVICE_NAME=$(basename `pwd`)
DIR=${GOPATH}/src/gitlab.com/gamerscomplete/CommandControl/modules/${SERVICE_NAME}
go build -o ${DIR}/${SERVICE_NAME} && rsync -a service.cfg ${DIR}/
