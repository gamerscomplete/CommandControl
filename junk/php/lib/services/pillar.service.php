<?PHP

class pillar {
/*
    Internode communication:

        start->
            start listening socket

        connect_to_network($node_ip, $node_port);


    Peers:
        network_id
        socket
        hostname
        last_communication
        services_running(array)
        ip
        establish_time
        running_as
        start_time
        resources
            disk(array)
                partitions/space
            memory
        system_uptime


    cli commands:
        show_peers [local|all] default=local
        change_network_id <new-network-id>
        stats
        connect <ip> <port>


    $command['show_peers']['usage'] = '[[local]|all]';
    $command['show_peers']['description'] = 'This command is used to display peers connect to the CommandControl network';
    $command['show_peers']['remote_command'] = 'show_peers';

    


*/
    private $logger;
    private $parent;
    private $peers = array();
    private $unauthed_peers = array();
    private $listen_socket;
    private $network_id;
    private $sync_timer = 30;
    private $start_time;
    private $services_running;

    private $queued_parent_commands = array();
    private $queued_peer_commands = array();

    function pillar() {
        $this->logger = new Logger("Pillar");
        $this->log("CommandControl Pillar service started");
    }

    function start() {
        $this->start_time = date("U");
        $this->start_listening_socket();
        $this->send_parent_message('set_network_id', $this->get_network_id());
//        usleep(10000);
        $this->send_parent_message('service_subscription', 'true');
        while(true) {
            $this->check_sockets();
            $this->check_sync();
            usleep(1000);
//            usleep(1000000);
        }
    }

    private function log($message, $level = 0) {
        $this->logger->log($message, $level);
    }


    private function build_cli_command_install() {
        $command['show_peers']['usage'] = '[[local]|all]';
        $command['show_peers']['description'] = 'This command is used to display peers connect to the CommandControl network';
        $command['show_peers']['remote_command'] = 'show_peers';

        $command['change_network_id']['usage'] = '<network-id>';
        $command['change_network_id']['description'] = 'Changes this nodes network id';
        $command['change_network_id']['remote_command'] = 'change_network_id';

        $command['stats']['usage'] = '';
        $command['stats']['description'] = 'Shows statistics Pillar collects';
        $command['stats']['remote_command'] = 'stats';

        $command['connect']['usage'] = '<ip> <port>';
        $command['connect']['description'] = 'Initiates connection to a peer';
        $command['connect']['remote_command'] = 'connect';
        return $command;
    }

    private function check_sync() {
        if(!count($this->peers)) {
            return;
        }
        foreach($this->peers as $key => $value) {
            if(!$value['last_syncd_to'] || ($value['last_syncd_to'] < (date("U") - $this->sync_timer))) {
                $this->sync_to_peer($key);
            }
        }
    }


    private function start_listening_socket() {
        $address = '0.0.0.0';
        $port = 10000;
        //listening
        if(($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
            $this->log("socket_create() failed: reason: " . socket_strerror(socket_last_error()), 4);
        }
        if (socket_bind($sock, $address, $port) === false) {
            $this->log("socket_bind() failed: reason: " . socket_strerror(socket_last_error($sock)), 4);
        }

        if (socket_listen($sock, 5) === false) {
            $this->log("socket_listen() failed: reason: " . socket_strerror(socket_last_error($sock)), 4);
        }
        $this->listen_socket = $sock;
        $this->log("Listening on $address:$port", 0);
    }

    private function shutdown() {
        if(!count($this->peers)) {
            exit(0);
            return;
        }
        foreach($this->peers as $key => $value) {
            $this->log("Closing socket to $key", 0);
            socket_close($value['fd']);
        }
        exit(0);
    }

    private function generate_guid() {
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .chr(125);// "}"
        return $uuid;
    }

    private function connect_to_network($params) {
        $params = explode(' ', $params);
        $this->log("Connecting to $params[0] $params[1]");
        if(($socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) and (socket_connect($socket, $params[0], $params[1]))) {
            //this cant happen. converted to unauthed peers and authed peers. need to exchange authentication at this point
            $identity = $this->build_identity();
            $identity['reply'] = 'false';
            $pieces = array();
            $pieces['ts'] = date("U");
            $pieces['sender'] = $this->get_network_id();
            $pieces['command'] = 'identify';
            $pieces['parameters'] = $identity;

            $message = serialize($pieces);
            if (socket_write($socket, $message, strlen($message)) === false) {
               $this->log("socket_write() failed. Reason: ".socket_strerror(socket_last_error($peer)), 3);
            }
            $this->unauthed_peers[] = $socket;
            //socket success
        } else {
            $this->log("Unable to connect to network. ".socket_strerror(socket_last_error()), 3);
        }
    }

//    function set_logger($logger_class) {
//        $this->logger = $logger_class;
//    }

    function check_sockets() {
        $read_sockets = array();
        if(count($this->parent)) {
            $read_sockets[] = $this->parent;
        }
        if(count($this->listen_socket)) {
            $read_sockets[] = $this->listen_socket;
        }
        if(count($this->peers)) {
            $peer_fds = array();
            foreach($this->peers as $peer) {
                $peer_fds[] = $peer['fd'];
            }
            $read_sockets = array_merge($read_sockets, $peer_fds);
        }
        if(count($this->unauthed_peers)) {
            $read_sockets = array_merge($read_sockets, $this->unauthed_peers);
        }

        $write = NULL;
        $exept = NULL;
        if(!count($read_sockets)) {
            $this->log("ran out of sockets. byebye", 3);
            die();
        }
        $sockets = socket_select($read_sockets, $write, $except, 0);
        if($sockets > 0) {
           foreach ($read_sockets as $input => $fd){
                if($fd == $this->parent) {
                    $recieved_message = socket_read($fd, 1024);
                    if(!$recieved_message) {
                        $this->log("Parent hung up. Exiting");
                        die();
                    } else {
                        //echo "Recieved message: $recieved_message\n";
                        $this->process_parent_message(unserialize($recieved_message));
                    }
                } else {
                    if(in_array($fd, $this->unauthed_peers)) {
                    //// MESSAGE FROM UNAUTHED. BETTER BE AUTH
                        $recieved_message = socket_read($fd, 1024);
                        if(!$recieved_message) {
                            if(!$this->remove_unauthed_peer($fd)) {
                                $this->log("Peer was not in list", 4);
                            }
                            return;
                        }
                        $peek = unserialize($recieved_message);
                        if($peek['command'] != 'identify') {
                            $this->log("Attempting commands unauthed. Command: $peek[command]", 4);
                            return;
                        }
                        $this->process_identity($peek['parameters'], $fd);
                    } elseif($this->lookup_peer_by_fd($fd)) {
                    //// MESSAGE FROM VALID PEER
                        $peer = $this->lookup_peer_by_fd($fd);
                        $recieved_message = socket_read($fd, 1024);
                        $this->peers[$peer]['bytes_in'] += strlen($recieved_message);
                        if(!$recieved_message) {
                            $this->log("Peer left us, cleaning up $peer");
                            //client left us. removing from peer list
                            unset($this->peers[$peer]);
                            return;
                        }
                        //echo "Processing peer message: $recieved_message from $peer\n";
                        $this->process_peer_message($recieved_message, $peer);
                    } else {
                    ///NO VALID PEERS. MUST BE NEW FRIENDZ!!!
                        if(($msgsock = socket_accept($fd)) === false) {
                            $this->log("socket_accept() failed: reason: " . socket_strerror(socket_last_error($sock)), 3);
                            break;
                        }
                        socket_getpeername($msgsock, $remote_address, $remote_port);
                        $this->log("Accepting new friend from $remote_address:$remote_port");
                        $this->unauthed_peers[] = $msgsock;
                    }
                }
            }
        }

        if(count($socket_peers) < 1) {
            return;
        }
        /*if(socket_select($socket_peers, $write, $except, 0) > 0) {
            foreach ($socket_peers as $input => $fd) {
                $recieved_message = socket_read($fd, 1024);
                $this->process_peer_message($recieved_message);
            }
        }*/
    }

    private function lookup_peer_by_fd($fd) {
        if(!count($this->peers)) {
            return false;
        }
        foreach($this->peers as $net_id => $peer) {
            if ($peer['fd'] == $fd) {
                return($net_id);
            }
        }
        return false;
    }


    private function remove_unauthed_peer($fd) {
        $iterator = 0;
        foreach($this->unauthed_peers as $peer) {
            if($peer === $fd) {
                unset($this->unauthed_peers[$iterator]);
                return true;
            }
            $iterator++;
        }
        return false;
    }

    private function get_network_id() {
        if(!$this->network_id) {
            $this->network_id = trim($this->generate_guid(), '{}');
        }
        return($this->network_id);
    }

    private function sync($fd) {
        $this->log("pillar:sync() not implemented", 3);
        /*
        */
        
    }

    private function build_identity() {
        $identity = array();
        $identity['network_id'] = $this->get_network_id();
        $identity['hostname'] = gethostname();
        $identity['services_running'] = $this->services_running;
        $identity['running_as'] = get_current_user();
        $identity['start_time'] = $this->start_time;
        return $identity;
    }

    private function process_identity($identify, $fd) {
        if(!$identify['network_id'] || !$identify['hostname'] || !$identify['running_as'] || !$identify['start_time']) {
            $this->log("Process identify failed. Not all required pieces provided", 4);
            //die(print_r($identify));
        }
        $this->log("Ident complete with node: $identify[network_id]");
        socket_getpeername($fd, $remote_address);
        $this->peers[$identify['network_id']]['hostname'] = $identify['hostname'];
        $this->peers[$identify['network_id']]['services_runnning'] = $identify['services_running'];
        $this->peers[$identify['network_id']]['running_as'] = $identify['running_as'];
        $this->peers[$identify['network_id']]['start_time'] = $identify['start_time'];
        $this->peers[$identify['network_id']]['fd'] = $fd;
        $this->peers[$identify['network_id']]['ip'] = $remote_address;

        if(in_array($fd, $this->unauthed_peers)) {
            $this->remove_unauthed_peer($fd);
        } else {
            $this->log("We dont know this peer that just identified with us", 4);
        }

        if($identify['reply'] == "false") {
            $this->log("Identing back");
            $identity = $this->build_identity();
            $identity['reply'] = 'true';
            $pieces = array();
            $pieces['ts'] = date("U");
            $pieces['sender'] = $this->get_network_id();
            $pieces['command'] = 'identify';
            $pieces['parameters'] = $identity;
            $message = serialize($pieces);
            $write = socket_write($fd, $message, strlen($message));
            if ($write === false) {
               $this->log("socket_write() failed. Reason: ".socket_strerror(socket_last_error($peer)), 4);
            } elseif($write < strlen($message)) {
                $this->log("We wrote less than we expected. Expected to write: ".strlen($message)." And wrote: ".$write." instead", 4);
            }
        }
//        print_r($this->peers);
    }

    private function broadcast_to_network($message) {
        $this->log("pillar::broadcast_to_network not yet implemented");
    }


    private function sync_from_peer($message, $peer) {
        //echo "Syncing from peer $peer\n";
        if(!$message['network_id'] || !$message['hostname'] || !$message['running_as'] || !$message['start_time']) {
            $this->log("Sync from peer '$peer' failed. Missing parts from sync message", 4);
            //print_r($message);
            return;
        }

        if(!$this->peers[$peer]) {
            $this->log("Sync from peer. Peer not known: '$peer'", 4);
            return;
        }

        $this->log("Syncing from peer: '$peer'", 0);

        $this->peers[$peer]['last_sync'] = date("U");
    }

    private function send_command_to_service($service, $node_id, $command, $params) {
        $sub_message = array();
        $sub_message['command'] = $command;
        $sub_message['parameters'] = $params;
        $sub_message['to']['service'] = $service;
        $sub_message['to']['network-id'] = $node_id;
        $sub_message['from']['service'] = 'pillar';
        $sub_message['from']['network-id'] = $this->network_id;

        $this->send_parent_message('send_to_service', $sub_message);
    }



    private function sync_to_peer($peer) {
        $sync_package['services_running'] = $this->services_running;
        $sync_package['network_id'] = $this->get_network_id();
        $sync_package['hostname'] = gethostname();
        $sync_package['running_as'] = get_current_user();
        $sync_package['start_time'] = $this->start_time;
        
        $this->send_peer_message('sync', $sync_package, $peer);
        $this->peers[$peer]['last_syncd_to'] = date("U");
        $this->log("Syncing to peer '$peer'", 0);
    }



    private function process_peer_message($message, $peer) {
        $message = unserialize($message);
//        echo "processing peer message. Command: '$message[command]' Parameters: $message[parameters]\n";
//        echo "Pillar processing peer message $message\n";
        switch($message['command']) {
            case '':
                return("No message provided");
                break;

            //specialty commands
            case 'test':
                echo "test message recieved by pillar with parameters $message[parameters]\n";
                return("success");
                break;
            case 'load':
//                echo "told to load $message[parameters]\n";
                $this->send_parent_message('load', $message['parameters']);
                break;
            //peer sync commands
            case 'sync':
                $this->sync_from_peer($message['parameters'], $peer);
                break;
            case 'send_to_service':
                $this->send_to_service($message['parameters']);
            default:
                return("Message not known");
                break;
        }
    }

    private function process_parent_message($message) {
//        $message = unserialize($message);
        //print_r($message);
//        $this->log("pillar processing parent message: $message", 0);
        switch($message['command']) {
            case 'shutdown':
                $this->shutdown();
                break;
            case 'connect':
                $this->connect_to_network($message['parameters']);
                break;
            case 'broadcast':
                $this->broadcast_to_network($message);
                break;
            case 'show_peers':
                //print_r($message);
                $this->show_peers($message['parameters'], $message['from']);
                break;
            case 'service_subscription':
//                echo "Pillar got service subscription\n";
//                print_r($message['parameters']);
                $this->services_running = $message['parameters'];
                $this->sync_to_peers();
                break;
            case 'send_to_service':
//                echo "send to service params: $message[parameters]\n";
                $this->send_to_service($message['parameters']);
                break;
            case 'send_message_to_peer':
                $this->peer_parent_pipe($message['parameters']);
//                $this->send_message_to_peer($message, $parameters, $peer);
                break;
            default:
                $this->log("Command unknown '$exploded_message[0]'", 4);
                break;
        }
    }


    private function show_peers($params, $from) {
    //die(print_r($from));
        if(count($this->peers)) {
            $iterator = 0;
            foreach($this->peers as $key => $value) {
                $new_line = '';
                if($iterator < count($this->peers)) {
                    $new_line = "\n";
                }
                $payload .= "Node: $key - IP: $value[ip] - Running as: $value[running_as] - Started: ".date("m/d/y h:i:s", $value[start_time])." - Services running: ".count($value['services_running'])." Net in/out: $value[bytes_in]B/$value[bytes_out]B".$new_line;
                $iterator++;
            }
        } else {
            $payload = "No known peers";
        }

//        $message['to']['network-id'] = $from['network-id'];
//        $message['to']['service'] = $from['service'];
        if(!$from['network-id']) {
            $this->log("No from network id provided to show_peers", 4);
            print_r($from);
            return;
        }
//        die(print_r($from));
        $this->send_command_to_service($from['service'], $from['network-id'], 'say', $payload);
    }

    private function send_to_service($params) {
//        die(print_r($params));
        if(in_array($params['to']['network-id'], array($this->network_id, 'local')) && $params['to']['service'] == 'pillar') {
            $this->log("Message was destined for us", 0);
//            print_r($params);
            $this->process_parent_message($params);
        } elseif(in_array($params['to']['network-id'], array($this->network_id, 'local'))) {
            $this->log("Message is destined for this node", 0);
            return($this->send_parent_message('send_to_service', $params));
        } elseif (!$this->peers[$params['to']['network-id']]) {
            //print_r($params);
            $this->log("Attempted to send message to unknown peer '".$params['to']['network-id']."'", 3);
            return(false);
        } else {
            $this->log("Sending message to peer with params $params", 0);
            return($this->send_peer_message('send_to_service', $params, $params['to']['network-id']));
        }
    }

    private function sync_to_peers() {
        //sync to all known peers
        if(!count($this->peers)) {
            return;
        }
        foreach($this->peers as $key => $value) {
            $this->sync_to_peer($key);
        }
    }

    private function peer_parent_pipe($message) {
        //die("Dumping peer pipe".print_r($message)."\n");
        //echo "Piping to peer from parent '$message[command]' '$message[parameters]' '$message[peer]'\n";
        $this->send_peer_message($message['command'], $message['parameters'], $message['peer']);
    }


    private function send_parent_message($command, $params) {
        $pieces = array();
        $pieces['ts'] = date("U");
//        $pieces['sender'] = $this->get_network_id();
        $pieces['command'] = $command;
        $pieces['parameters'] = $params;

        $message = serialize($pieces);

        $read = NULL;
        $except = NULL;
        $writeable[0] = $this->parent;

        $writeable_sockets = socket_select($read, $writeable, $except, 0);
        if($writeable_sockets > 0) {
            $write = socket_write($this->parent, $message, strlen($message));
            if ($write === false) {
                $this->log("socket_write() failed. Reason: ".socket_strerror(socket_last_error($this->parent)), 4);
                exit(1);
            } elseif($write < strlen($message)) {
                $this->log("We wrote less than we expected. Expected to write: ".strlen($message)." And wrote: ".$write." instead", 4);
            } else {
                $this->log("Succesfully sent message to parent. Command: $command Bytes: $write", 0);
            }
        } elseif($writeable_sockets === false) {
            $this->log("Write socket select failed for send_parent_message", 4);
        } else {
            $this->log("No writeable sockets :(", 4);
        }
    }

    private function send_peer_message($command, $parameters, $peer) {
        $pieces = array();
        $pieces['ts'] = date("U");
        $pieces['sender'] = $this->get_network_id();
        $pieces['command'] = $command;
        $pieces['parameters'] = $parameters;
        //die(print_r($peer));
        //die(print_r($this->peers));
        $message = serialize($pieces);
//        echo "Sending message to peer...\n";
//        print_r($this->peers[$peer]);
        if(!$this->peers[$peer]['fd']) {
            $this->log("Attempting to send to peer that doesnt exist '$peer'", 3);
        }

        $socket_write = socket_write($this->peers[$peer]['fd'], $message, strlen($message));
        if ($socket_write === false) {
            $this->log("socket_write() failed. Reason: ".socket_strerror(socket_last_error($this->peers[$peer]['fd'])), 4);
            //peer left us so remove them
            unset($this->peers[$peer]);
        } elseif($socket_write != strlen($message)) {
            $this->log("Socket did not write full message to peer", 4);
        } else {
            $this->peers[$peer]['bytes_out'] += $socket_write;
        }
    }

    public function set_parent_socket($socket) {
        $this->parent = $socket;
    }

}
