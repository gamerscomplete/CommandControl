<?PHP

class database {

//This will be the internal distributed CommandControl database distributed between nodes

    function database() {
        echo "Database initializing...\n";
    }

    private function create_database($name, $redundancy) {
        echo "Creating database $name with redundancy of $redundancy\n";
    }

    private function create_table($name, $dbname) {
    }

    private function insert_record($db_name, $table, $record) {
    }

    private function delete_record($db_name, $table, $record_id) {
    }

    private function update_record($db_name, $table, $record_id) {
    }

    private function select_record($db_name, $query) {
        echo "DB: $db_name\nQuery: $query\n";
    }

    private function sync() {
        //
    }

    private function connect_to_cluster() {
        //open socket connection to other nodes. 
    }
}
