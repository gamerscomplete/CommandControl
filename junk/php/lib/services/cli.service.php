<?PHP

class cli {
    public $required_modules = array();
    private $alias_db_file = '.alias.db';
    
    private $logger;
    private $stdin;
    private $parent;
    private $prompt = ": ";
    private $command_tree = array();
    private $network_id;

    private $aliases = array();

    function cli() {
        $this->required_modules[] = "logger";
    }

    function start() {
        $this->logger = new Logger("CLI");
        $this->say("Welcome to the CommandControl CLI");
        $this->load_alias_db();
        $this->send_parent_message('get_network_id', '');
//        $this->say("$this->prompt ");
        while(true) {
            $this->check_sockets();
            usleep(1000);
        }
    }

    private function log($message, $level = 0) {
        $this->logger->log($message, $level);
    }

    private function install_commandset($commands) {
        if(!is_array($commands)) {
            $this->say("Unable to install commands. Command set not array\n");
        }
        foreach($commands as $parts => $command) {
            if($this->command_tree[$command]) {
                //command already in command tree
            } else {
                //add to command tree
            }

            if(is_array($parts['subcommands'])) {
                
                //we have sub commands to process through
                foreach($parts['subcommands'] as $subcommand) {
                    //process subcommand
                }
            }
        }
    }

    private function install_command($tree, $command) {
    }

    private function send_command_to_service($service, $node_id, $command, $params) {
        $sub_message = array();
        $sub_message['command'] = $command;
        $sub_message['parameters'] = $params;
        $sub_message['to']['service'] = $service;
        $sub_message['to']['network-id'] = $node_id;
        $sub_message['from']['service'] = 'cli';
        $sub_message['from']['network-id'] = $this->network_id;


//        echo "Sending to parent for service: ".print_r($sub_message)."\n";
        $this->send_parent_message('send_to_service', $sub_message);
    }

    private function command_send_command_to_service($command) {
//        echo "command send: ".print_r($command)."\n";
        $exploded_command = explode(' ', $command);
        $params = $exploded_command;
        unset($params[0], $params[1], $params[2]);
        $this->send_command_to_service($exploded_command[0], $exploded_command[1], $exploded_command[2], $params);
    }

    private function change_prompt($new_prompt) {
        $this->prompt = $new_prompt;
    }

    private function check_sockets() {
        if(!$this->stdin) {
            $this->say("No stdin\n");
        } elseif(!$this->parent) {
            $this->say("no parent\n");
        }
        $stream_read[] = $this->stdin;
        $socket_read[] = $this->parent;
        $write = NULL;
        $exept = NULL;

        //handle stdin
        if (stream_select($stream_read, $write, $except, 0) > 0) {
            //echo "Processing ".count($stream_read)." streams\n";
            foreach ($stream_read as $input => $fd){
                $this->process_input(trim(fgets($fd)));
                echo $this->prompt;
            }
        }

        //handle parent
        if(socket_select($socket_read, $write, $except, 0) > 0) {
           foreach ($socket_read as $input => $fd){
                $recieved_message = socket_read($fd, 1024);
                if(!$recieved_message) {
                    $this->say("Parent hung up. Exiting\n");
                    die();
                } else {
                    $this->process_parent_message(unserialize($recieved_message));
                    //$this->say($this->prompt." ");
                }
            }
        }
    }

    private function send_parent_message($command, $params) {
        $pieces = array();
        $pieces['ts'] = date("U");
        $pieces['command'] = $command;
        $pieces['parameters'] = $params;

        $message = serialize($pieces);

        $socket_write = socket_write($this->parent, $message, strlen($message));

        if ($socket_write === false) {
           $this->say("socket_write() failed. Reason: ".socket_strerror(socket_last_error($this->parent)));
        } elseif($socket_write < strlen($message)) {
            $this->log("Unable to send complete message to parent '$message'", 0);
        } else {
            $this->log("Socket write to parent successful with $socket_write bytes", 0);
        }
    }

    public function set_stdin($socket) {
        $this->stdin = $socket;
   }

    public function set_parent_socket($socket) {
        $this->parent = $socket;
    }


    private function process_parent_message($message) {
//        $message = unserialize($message);
//        $this->log("pillar processing parent message: $message", 0);
        switch($message['command']) {
            case 'service_subscription':
                $this->services_running = $message['parameters'];
                $this->sync_to_peers();
                break;
            case 'say':
                $this->say("\n".$message['parameters']);
                break;
            case 'shutdown':
                $this->shutdown();
                break;
            case 'set_network_id':
                $this->network_id = $message['parameters'];
                break;
            case 'network_id_subscription':
                $this->network_id = $message['parameters'];
                break;
            case 'send_to_service':
//                echo "Recieved send_to_service\n";
//                print_r($message['parameters']);
                $this->process_parent_message($message['parameters']);
                break;
            default:
                $this->say("Parent sent unknown command: $message[command]");
                break;
        }
    }


    private function process_input($input) {
        $this->log("Processing command: '$input'", 0);
        $exploded_input = explode(' ', $input);
        $command = $exploded_input[0];
        unset($exploded_input[0]);
        if($this->aliases[$command]) {
            $passed_params = '';
            if(count($exploded_input)) {
                $passed_params = " ".implode(' ', $exploded_input);
            }
            $input = $this->aliases[$command].$passed_params;
            $this->log("aliasing '$command' to '".$this->aliases[$command]."' New command: '$input'", 0);
            $exploded_input = explode(' ', $input);
            $command = $exploded_input[0];
            unset($exploded_input[0]);
        }
        $params = implode(' ', $exploded_input);

//        echo "Command: '$command' Params: '$params'";
        if(!$command) {
            return;
        }
        switch ($command) {
            case '':
                return;
                break;
            case 'die':
                $this->send_parent_message('shutdown', '');
//                $this->say("Command line terminating. Goodbye");
//                die();
                break;
            case 'help':
                $this->say("help command not created yet");
                break;
            case 'server':
                $command = $exploded_input[1];
                unset($exploded_input[1]);
                $new_params = implode(" ", $exploded_input);
                $this->send_parent_message($command, $new_params);
                break;
            case 'alias':
                $this->alias($params);
                break;
            case 'dump_aliases':
                //print_r($this->aliases);
                break;
            case 'network-id':
                if(!$this->network_id) {
                    $this->send_parent_message('get_network_id', '');
                    $this->say("Requesting network-id");
                } else {
                    $this->say("Network ID: '".$this->network_id."'");
                }
                break;
            case 'service':
                $this->command_send_command_to_service($params);
                break;
            default;
                $this->say("Command '$input' not known");
        }
    }

    private function alias($input) {
        $exploded_input = explode(' ', $input);
        $command = $exploded_input[0];
        unset($exploded_input[0]);
        $params = implode(' ', $exploded_input);
        if($command == 'create') {
            $this->create_alias($params);
        } elseif($command == 'delete') {
            $this->delete_alias($params);
        } elseif($command == 'show') {
            if(!count($this->aliases)) {
                $this->say("No aliases");
                return;
            }
            foreach($this->aliases as $key => $value) {
                $aliases .= "$key - $value\n";
            }
            $this->say($aliases);
        } else {
            $this->say("Alias command not known '$command'");
        }
    }

    private function delete_alias($alias) {
        if(!$this->aliases[$alias]) {
            $this->say("Alias '$alias' did not exist");
            return;
        } else {
            unset($this->aliases[$alias]);
            $this->write_alias_db();
        }
    }

    private function load_alias_db() {
        if(file_exists($this->alias_db_file)) {
            $aliasdb = unserialize(file_get_contents($this->alias_db_file));
            $this->aliases = $aliasdb;
        }
    }

    private function write_alias_db() {
        file_put_contents($this->alias_db_file, serialize($this->aliases));
    }

    private function create_alias($params) {
        //usage: "alias newcommand string of parameters"
        //ex: "alias connect server connect 10.1.10.33 10000
        //ex: "alias connect
        $param_array = explode(' ', $params);
        if(count($param_array) < 2) {
            $this->say("alias creation failed. Not enough parameters. '$params'");
            return false;
        }
        $alias_to = $param_array[0];
        unset($param_array[0]);
        $alias_from = implode(' ', $param_array);
        $this->aliases[$alias_to] = $alias_from;
        $this->write_alias_db();
    }

    private function say($message) {
        echo $message."\n";
    }

    private function shutdown() {
//        $this->say("Got told to shutdown. byebye\n");
        exit(0);
    }


/*
/////////////////////////// JUNKYARD


    function get_input($prompt = ":") {
        readline_completion_function(array('cli', 'autocomplete'));
        $input = readline("$prompt ");
        readline_add_history($input);
        return($input);
    }

    function autocomplete($input, $index) {
        $rl_info = readline_info();

//        $full_input = substr($rl_info['line_buffer'], 0, $rl_info['end']);
        $full_input = substr($input, 0, $rl_info['end']);
//        print_r($rl_info);
        $matches = array();
//        echo "\nAutocomplete; Full input: '$full_input' Input: '$input' Index: '$index' Line buffer: '".$rl_info['line_buffer']."'\n";
        if($full_input == 'fu') {
            $matches[] = substr('fuck', $index);
        }
        
        return $matches;
    }





    function xreadline($prompt)
    {
        global $xreadline, $xreadline_line;
        $code = '$GLOBALS["xreadline"] = false;' .
                '$GLOBALS["xreadline_line"] = $line;' .
                'readline_callback_handler_remove();';
        $cb = create_function('$line', $code);
        readline_callback_handler_install($prompt, $cb);
        $signal = defined("SIGWINCH") ? SIGWINCH : 28;
        posix_kill(posix_getpid(), $signal);
        $xreadline = true;
        while ($xreadline)
            readline_callback_read_char();
        return is_null($xreadline_line) ? false : $xreadline_line;
    }*/
}

//$cli = new cli();
