<?PHP

class test {
    public $required_modules = array();
    
    var $logger;
    private $stdin = array();
    private $parent = array();

    function test() {
        echo "Welcome to the CommandControl test service\n";
        $this->required_modules[] = "logger";
    }

    function start() {
        while(true) {
            $this->check_sockets();
            usleep(100000);
        }
    }

    function set_logger($logger_class) {
        $this->logger = $logger_class;
    }

    function check_sockets() {
        if(!$this->parent) {
            echo "no parent\n";
        }
        $socket_read = $this->parent;
        $write = NULL;
        $exept = NULL;

        //handle parent
        if(socket_select($socket_read, $write, $except, 0) > 0) {
           foreach ($socket_read as $input => $fd){
                $recieved_message = socket_read($fd, 1024);
                if(!$recieved_message) {
                    echo "Parent hung up. Exiting\n";
                    die();
                } else {
//                    echo "Server response: '$recieved_message'\n";
                    echo "\n$recieved_message\n";
                    echo $this->prompt." ";
                }
            }
        }
    }

    private function send_parent_message($message) {
        if (socket_write($this->parent[0], $message, strlen($message)) === false) {
           echo "socket_write() failed. Reason: ".socket_strerror(socket_last_error($this->parent[0]));
        }
    }

    public function set_parent_socket($socket) {
        $this->parent[0] = $socket;
    }

}
