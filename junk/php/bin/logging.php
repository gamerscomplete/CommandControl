<?PHP

class Logger {

/*    function Logger() {
        //echo "Logger module loaded!\n";
    }
*/

    private $logging_level;
    private $levels;

    function __construct() {
        $this->set_levels();
        $this->set_level_from_config();
        $args = func_get_args();
        switch (func_num_args()) {
            case 1:
                openlog($args[0], LOG_PID | LOG_PERROR, LOG_LOCAL7);
                $this->log("Logging module initialized with name $args[0]", 0);
                break;
            case 2:
                openlog($args[0], LOG_PID | LOG_PERROR, $args[1]);
                $this->log("Logging module initialized with name $args[0] and custom facility", 0);
                break;
            case 3:
                if($args[2] === 'false') {
                    openlog($args[0], LOG_PID, $args[1]);
                    $this->log("Logging module initialized with name $args[0] and display logs off", 0);
                } else {
                    openlog($args[0], LOG_PID | LOG_PERROR, $args[1]);
                }
                break;
            default:
                openlog("CommandControl", LOG_PID | LOG_PERROR, LOG_LOCAL7);
                $this->log("Logging module initialized.", 0);
                break;
        }
    }

    private function set_level_from_config() {
        if(defined('LOGGING_LEVEL')) {
            foreach($this->levels as $key => $value) {
                if($value['short'] == LOGGING_LEVEL) {
                    $this->logging_level = $key;
                    $this->log("Setting logging level to $key", 0);
                    return;
                }
            }
            if(!$this->logging_level) {
                $this->log("Never found log level in config", 3);
                $this->logging_level = 0;
            }
        } else {
            $this->logging_level = 0;
            $this->log('Logging level not defined in config. Defaulting to debug', 3);
        }
    }

    private function set_levels() {
        $loglevel[0]['level'] = LOG_DEBUG;
        $loglevel[0]['name'] = 'DEBUG';
        $loglevel[0]['short'] = 'DEBUG';
        $loglevel[1]['level'] = LOG_INFO;
        $loglevel[1]['name'] = 'INFO';
        $loglevel[1]['short'] = 'INFO';
        //normal, but significant, condition
        $loglevel[2]['level'] = LOG_NOTICE;
        $loglevel[2]['name'] = 'NOTICE';
        $loglevel[2]['short'] = 'NOTICE';
        //warning conditions
        $loglevel[3]['level'] = LOG_WARNING;
        $loglevel[3]['name'] = 'WARNING';
        $loglevel[3]['short'] = 'WARN';
        //error conditions
        $loglevel[4]['level'] = LOG_ERR;
        $loglevel[4]['name'] = 'ERROR';
        $loglevel[4]['short'] = 'ERR';
        $loglevel[5]['level'] = LOG_CRIT;
        $loglevel[5]['name'] = 'CRITICAL';
        $loglevel[5]['short'] = 'CRIT';
        //action must be taken immediately
        $loglevel[6]['level'] = LOG_ALERT;
        $loglevel[6]['name'] = 'ALERT';
        $loglevel[6]['short'] = 'ALERT';
        //system is unusable
        $loglevel[7]['level'] = LOG_EMERG;
        $loglevel[7]['name'] = 'EMERGENCY';
        $loglevel[7]['short'] = 'EMERG';
        $this->levels = $loglevel;
    }

    function log($message, $level = 0) {
        if($level > 7) {
            $this->log("Attempted to log with level greater than 7. Level: '$level'", 4);
            $level = 7;
        }
        if($level < $this->logging_level) {
            return;
        }
        $date = date('Y/m/d/H:i:s');
        //Set the loglevel based on the level defined or default to DEBUG, then post message to syslog
        //  syslog($loglevel[$level], $message);
        $message = "$date - [".$this->levels[$level]['name']."]$message";
        //echo "Logging: $message\n";
        syslog($this->levels[$level]['level'], $message);
    }

    public function closelog() {
        closelog();
    }
}
