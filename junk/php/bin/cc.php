<?PHP

require_once("../etc/config.php");
require_once("argparser.php");
require_once("logging.php");

/*
 * NEW AND IMPROVED BABY!!
 */

class CommandControl {

    var $start_time;

    //Classes
    public $services_running = array();
    private $service_db = array();
    private $logger;
    private $service_sockets = array();
    private $stdin;
    private $service_subscribers = array();
    private $network_id;

    private $queued_service_commands = array();


    function CommandControl() {
        $this->logger = new Logger("CommandControl");
        $this->start_time = date("U");
        $this->log("CommandControl starting up");

        $this->log("Loading service database...");
        $this->load_service_database();
        $this->log("Service database loaded");
        $this->install_signal_handler();
        $this->stdin = fopen('php://stdin', 'r'); //open STDIN for reading

    }

    function check_service_loaded($service) {
        if($this->services_running[$service]) {
            return true;
        } else {
            return false;
        }
    }

    function log($message, $level = 0) {
        $this->logger->log($message, $level);
    }

    private function notify_service_subscribers() {
        $this->log("Notifying service subscribers of new service changes", 0);
        if(!count($this->service_subscribers)) {
            $this->log("No service subscribers", 0);
            return;
        }
        foreach($this->service_subscribers as $subscriber) {
            $this->log("Sending service subscription to '$subscriber'", 0);
            $this->send_child_message('service_subscription', $this->services_running, $subscriber);
        }
    }

    public function start_service($service_name) {
        if(!$service_name) {
            $this->log("start_service failed with no name provided '$service_name'", 4);
            return false;
        }
        if($this->check_service_loaded($service_name)) {
            $this->log("Service already running: $service_name", 3);
            return false;
        }
        //Starting out really simple with just doing a simple require_once on a service. This will need extensive upgrades down the road
        if(!defined('SERVICES_DIRECTORY')) {
            $this->log("Service directory not defined. Service load failed.", 4);
            return false;
        } elseif(!file_exists(SERVICES_DIRECTORY."/$service_name.service.php")) {
            $this->log("Service $service_name.service.php does not exist", 4);
            return false;
        }

        $this->log("Loading service: $service_name", 1);

        //Some simple security to make sure files were as generated. This will need much better security
        if(!$this->service_db[$service_name]) {
            $this->log("Service $service_name not in service database", 4);
            return false;
        }
        if($this->service_db[$service_name]['hash'] != md5(file_get_contents(SERVICES_DIRECTORY.'/'.$service_name.'.service.php'))) {
            $this->log("Service $service_name does not match service database.", 4);
            return false;
        }

        if (socket_create_pair(AF_UNIX, SOCK_STREAM, 0, $socket_pair) === false) {
            $this->log("socket_create_pair() failed. Reason: ".socket_strerror(socket_last_error()), 4);
            return false;
        }

        $pid = pcntl_fork();

        if($pid == -1) {
            $this->log("Could not fork process.", 4);
        } elseif($pid) {
            //parent
            socket_close($socket_pair[0]);
            $this->log("Spawned child for service $service_name with pid: $pid", 0);
            $this->services_running[$service_name]['fd'] = $socket_pair[1];
            $this->services_running[$service_name]['start_time'] = date("U");
            $this->services_running[$service_name]['bytes_in'] = 0;
            $this->services_running[$service_name]['bytes_out'] = 0;
            $this->services_running[$service_name]['messages_sent'] = 0;
            $this->services_running[$service_name]['messages_recieved'] = 0;
            $this->services_running[$service_name]['pid'] = $pid;
            $this->notify_service_subscribers();
        } else {
            //child
            if(!include_once(SERVICES_DIRECTORY."/$service_name.service.php")) {
                $this->log("Could not include service $service_name", 4);
                return false;
            }
            $service = new $service_name();
            if(method_exists($service, 'set_parent_socket')) {
                $service->set_parent_socket($socket_pair[0]);
            }
            if(method_exists($service, 'set_stdin')) {
                $service->set_stdin($this->stdin);
            }
        
            $service->start();
        }
        return true;
    }


    function check_sockets() {
        if(count($this->services_running) < 1) {
            $this->log("Attempting to check sockets with no services running", 0);
//            echo "No service sockets\n";
            return;
        }
        $read_array = array();
        foreach($this->services_running as $key => $value) {
            if($value['fd']) {
                $read_array[$key] = $value['fd'];
            }
        }

        if(!count($read_array)) {
            $this-log("No sockets to read from", 4);
        }

        $write = NULL;
        $except = NULL;

        if (socket_select($read_array, $write, $except, 0) > 0){
            foreach ($read_array as $input => $fd) {
                $recieved_message = socket_read($fd, 1024000);
                $this->log("Got readable sockets. Num sockets: ".count($read_array), 0);
                //$this->log("Read bytes: ".strlen($recieved_message), 0);
                if(!$recieved_message) {
                    $this->log("Service '$input' terminated", 0);
                    socket_close($fd);
                    unset($this->services_running[$input]);
                } else {
                    $this->log("Got message from child: '$recieved_message'", 0);
                    $service_name = $this->lookup_service_by_fd($fd);
                    if($service_name) {
                        $this->log("Found service. Processing message ");
                        $this->services_running[$service_name]['messages_recieved']++;
                        $this->services_running[$service_name]['bytes_in'] += strlen($recieved_message);
                        $serial=str_replace('{', '|', $recieved_message);
                        $serial=str_replace('}', '|', $serial);

                        $p = explode('|', $serial);

                        $total = count($p);
                        $arrr=array();
                        for($i=0; $i<=$total; $i++){
                            if(strlen($p[$i])>1){
                                $arrr[$i] = $p[$i] . "{" . $p[$i+ 1]  . "}";
                                $i++;
                            }
                        }
                        foreach($arrr as $key => $value){
                            $this->log("Sending to command processor '".unserialize($value), 0);
                            $this->command_processor(unserialize($value), $service_name);
                        }
                    } else {
                        $this->log("Lookup failed for service. This should never happen", 4);
                    }
                }
            }
        }
    }

    private function lookup_service_by_fd($fd) {
        foreach($this->services_running as $key => $value) {
            if($value['fd'] == $fd) {
                return $key;
            }
        }
        //never found the key
        return false;
    }



    private function send_child_message($command, $parameters, $service_name) {
        if(!$this->services_running[$service_name]) {
            //print_r($this->services_running);
            $this->log("Attempted to send message to '$service_name' but it is not running", 3);
            return false;
        }
        $pieces = array();
        $pieces['ts'] = date("U");
//        $pieces['sender'] = $this->get_network_id();
        $pieces['command'] = $command;
        $pieces['parameters'] = $parameters;

        $message = serialize($pieces);

        $socket_write = socket_write($this->services_running[$service_name]['fd'], $message, strlen($message));
        if ($socket_write === false) {
            $this->log("socket_write($command, $parameters, **) failed. Reason: ".socket_strerror(socket_last_error($fd)));
            return false;
        } elseif($socket_write != strlen($message)) {
            $this->log("Socket did not write exact amount of bytes requested. Requested: '".strlen($message)."' Sent: '$socket_write'", 4);
            return false;
        } else {
            $this->services_running[$service_name]['messages_sent']++;
            $this->services_running[$service_name]['bytes_out'] += $socket_write;
            return true;
        }
    }

    private function list_available_services() {
        $iterator = 0;
        $service_count = count($this->service_db);
        $list = "Service count: $service_count\n";
        foreach($this->service_db as $name => $service) {
            $iterator++;
            if($service_count > $iterator) {
                $new_line = "\n";
            }
            $running = '';
            if($this->services_running[$name]) {
                $running = " Socket IN/OUT ".$this->services_running[$name]['bytes_in']."B/".$this->services_running[$name]['bytes_out']."B ";
                $running .= "Messages IN/OUT: ".$this->services_running[$name]['messages_recieved']."/".$this->services_running[$name]['messages_sent'];
                $running .= "[RUNNING]";
            }
            $list .= "$iterator> $name $service[filename] $service[size] Bytes Version: $service[version]$running$new_line";
            $new_line = '';
        }
        return $list;
    }

    private function shutdown() {
        foreach($this->services_running as $key => $value) {
            $this->send_child_message('shutdown','', $key);
        }
        exit(0);
    }

    private function connect_to_node($parameters) {
        //send connect message to pillar
        $this->send_child_message('connect', $parameters, 'pillar');
    }

    private function send_to_service($params) {
        //data integrity check
        if(!$params['to']['service']) {
            $this->log("No service parameter provided for send to service", 4);
        } elseif(!$params['from']['network-id']) {
            $this->log("Attempting to send to service but no network-id provided from sending service", 4);
        } elseif(!$params['to']['network-id']) {
            $this->log("No destination network-id to send to", 4);
        } elseif(!$params['parameters']) {
            $this->log("Message did not contain params", 4);
        } elseif(!$params['command']) {
            $this->log("No command provided for send to service", 4);
        }

        if(in_array($params['to']['network-id'], array($this->network_id, 'local'), true)) {
            //deliver locally
            if(!$this->check_service_loaded($params['to']['service'])) {
                $this->log("Attempted to send a message to a service that is not running. Service: '$params[service]'", 3);
                return false;
            } else {
//                die(print_r($params));
                $this->log("Messaged destined locally. delivering to local service ".$params['to']['service'], 0);
//                print_r($params);
                $this->send_child_message('send_to_service', $params, $params['to']['service']);
            }
        } else {
            //print_r($params);
            $this->send_child_message('send_to_service', $params, 'pillar');
            $this->log("Forwarding send_to_service to Pillar for injecting to the network", 0);
//            $this->log("Forward to outside not not yet implemented", 3);
            //dump it to pillar to forward to proper node
        }
    }

    private function command_processor($input, $service) {
        echo "\n";
        print_r($input);
        echo "\n";
//        $input = unserialize($input);
//        if(count($input) > 1) {
 //           echo "Recieved more than 1 command\n";
  ///      }
//        $this->log("CC processing command: '".$input['command']."' from service '$service' to ".$input['to']['service'], 0);
        switch($input['command']) {
            case 'send_to_service':
                $this->send_to_service($input['parameters']);
                break;
            case 'load':
                echo "Starting service: '$input[parameters]'\n";
                if($this->start_service($input['parameters'])) {
                    $this->send_child_message('say', "'$input[parameters]' service loaded succesfully", $service);
                } else {
                    $this->send_child_message('say', "'$input[parameters]' service failed to load", $service);
                }
                break;
            case 'list':
                $this->send_child_message('say', $this->list_available_services(), $service);
                break;
            case 'set_network_id':
                $this->log("Setting network id to '$input[parameters]'", 0);
                $this->network_id = $input['parameters'];
                break;
            case 'get_network_id':
                $this->send_child_message('set_network_id', $this->network_id, $service);
                break;
            case 'connect':
                //issue pillar command to connect. this is ugly and disapoints me and I havnt even written it yet...
                $this->connect_to_node($input['parameters']);
                break;
            case 'service_subscription':
                $this->service_subscribers[] = $service;
                $this->send_child_message('service_subscription', $this->services_running, $service);
                break;
            case 'shutdown':
                $this->log("Shutdown issued to cc\n", 0);
                $this->shutdown();
                break;
            default:
                $return = 'Unknown server command';
                break;
        }
        return $return;
    }

    private function get_status() {
        $message = 'Services loaded: '.count($this->service_sockets).'';
        return $message;
    }

    function load_service_database() {
        if(!defined('SERVICES_DATABASE')) {
            $this->log('SERVICES_DATABASE not defined in configuration file');
            die();
        }
        if(!is_file(SERVICES_DATABASE)) {
            echo "Service database file does not exist\n";
            die();
        }
        $service_db = unserialize(file_get_contents(SERVICES_DATABASE));
        if(!is_array($service_db)) {
            echo "Service database not valid\n";
        }
        $this->service_db = $service_db;
    }

    function install_signal_handler() {
        //pcntl_signal(SIGTERM, array('CommandControl', 'sig_handler'));
        //pcntl_signal(SIGINT, array('CommandControl', 'sig_handler'));
        //pcntl_signal(SIGCHLD, array('CommandControl', 'sig_handler'));
    }
    
    function sig_handler($sig) {
        switch($sig) {
            case SIGTERM:
                echo "Caught sigterm. Exiting\n";
                exit();
            case SIGINT:
                echo "Caught sigint. Exiting\n";
                exit();
                break;
            case SIGCHLD:
                pcntl_waitpid(-1, $status);
                break;
            default:
                break;
        }
    }
}

$commander = new CommandControl();
$args = new Args();

if($args->flag('s')) {
//    echo "Loading services...\n";
    $services_to_load = explode(",", $args->flag('s'));
    foreach ($services_to_load as $service) {
        if(!$commander->start_service($service)) {
            //echo "Service startup failed for $service\n";
        }
    }
    while(true) {
        $commander->check_sockets();
        usleep(1000);
    }
    echo "Done.\n";
}
