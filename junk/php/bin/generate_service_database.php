<?PHP

require_once("../etc/config.php");

if(!defined('SERVICES_DIRECTORY')) {
    die("SERVICES_DIRECTORY not defined in configuration file\n");
} elseif(!defined('SERVICES_DATABASE')) {
    die("SERVICES_DATABASE not defined in configuration file\n");
} elseif(!$argv[1]) {
    die("Servicesx directory to generate from not given\n");
}

//check for existing db
if(is_file(SERVICES_DATABASE)) {
    echo "Service db already exists. Updating\n";        
    $db = unserialize(file_get_contents(SERVICES_DATABASE));
//    print_r($db);
}



echo "Generating service database...\n";

$services = glob("$argv[1]/*.service.php");

$service_db = array();


if(!count($services)) {
    die("No services found in $argv[1]\n");
}
foreach($services as $service) {
    $service_name = basename($service);
    $md5 = md5(file_get_contents($service));
    $exploded_name = explode(".", $service_name);
    $name = $exploded_name[0];
    $service_db[$name]['hash'] = $md5;
    $service_db[$name]['timestamp'] = date("U");
    $service_db[$name]['filename'] = $service_name;
    $service_db[$name]['size'] = filesize($service);
    if($db[$name]) {
        if($db[$name]['hash'] != $md5) {
            $version = $db[$name]['version'] + 1;
            echo "Module $name updated to version $version with hash - $md5\n";
        } else {
            $version = $db[$name]['version'];
        }
    } else {
        echo "New module added: $name with hash - $md5\n";
        $version = 1;
    }
    $service_db[$name]['version'] = $version;
}

echo "Writing service database...\n";
file_put_contents(SERVICES_DATABASE, serialize($service_db));
echo "Service database written\n";
