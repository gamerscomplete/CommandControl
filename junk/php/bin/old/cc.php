#!/usr/bin/php
<?PHP

$__server_listening = true;

//Set our include path
set_include_path('/home/chris/checkout/CommandControl/php/lib:/home/chris/checkout/CommandControl/php/etc');

// don't timeout
set_time_limit (0);

/* Turn on implicit output flushing so we see what we're getting as it comes in. */
ob_implicit_flush();

declare(ticks = 1);

require_once("config.php");
require_once("functions.php");

echo "Installing signal handler...\n";
// setup signal handlers
pcntl_signal(SIGTERM, 'sig_handler');
pcntl_signal(SIGINT, 'sig_handler');
pcntl_signal(SIGCHLD, 'sig_handler');

/* nobody/nogroup, change to your host's uid/gid of the non-priv user */ 
//change_identity(65534, 65534);

openlog(LOG_NAME, LOG_PID | LOG_PERROR, LOG_LOCAL0);

//send_log("COMMAND CONTROL INITIALIZING...",2);
echo "COMMAND CONTROL INITIALIZING...\n";

server_loop(SERVER_ADDRESS, SERVER_PORT);


function server_loop($address, $port) {
    GLOBAL $__server_listening;

    if(($sock = socket_create(AF_INET, SOCK_STREAM, 0)) < 0) {
        echo "failed to create socket: ".socket_strerror($sock)."\n";
        exit();
    }

    if(($ret = socket_bind($sock, $address, $port)) < 0) {
        echo "failed to bind socket: ".socket_strerror($ret)."\n";
        exit();
    }

    if( ( $ret = socket_listen( $sock, 0 ) ) < 0 ) {
        echo "failed to listen to socket: ".socket_strerror($ret)."\n";
        exit();
    }

    socket_set_nonblock($sock);

    echo "waiting for clients to connect\n";

    while ($__server_listening) {
        $connection = @socket_accept($sock);
        if ($connection === false) {
            usleep(100);
        } elseif ($connection > 0) {
            handle_client($sock, $connection);
        } else {
            echo "error: ".socket_strerror($connection);
            die;
        }
    }
}

function sig_handler($sig) {
    switch($sig) {
        case SIGTERM:
            echo "Ahhhh shit they killed us\n";
        case SIGINT:
            exit();
        break;

        case SIGCHLD:
            pcntl_waitpid(-1, $status);
        break;
    }
}

function handle_client($ssock, $csock) {
    GLOBAL $__server_listening;

    $pid = pcntl_fork();

    if ($pid == -1) {
        /* fork failed */
        echo "fork failure!\n";
        die;
    } elseif ($pid == 0) {
        /* child process */
        $__server_listening = false;
        socket_close($ssock);
        interact($csock);
        socket_close($csock);
    } else {
        socket_close($csock);
    }
}

function interact($socket) {
    echo "In client code\n";
    $welcome_message = "ola beeches\n";
    socket_write($socket, $welcome_message, strlen($welcome_message));

    while (!$terminate) {
        $buf =  substr(socket_read($socket, 2048), 0, -2);
//        $buf = @socket_read($socket, 2048, PHP_NORMAL_READ);
        if($buf === false) {
            echo "Client bailed on us\n";
            socket_close($socket);
            die();
        } elseif ($buf != '') {
            echo "Recieved from client: '$buf'\n";
            if($buf == "TERM") {
                $goodbye_message = "Goodbye long lost friend\n";
                socket_write($socket, $goodbye_message, strlen($goodbye_message));
                $terminate = true;
            }

            $msg_return = process_message($buf);
        }
    }
}


/////////////////////////////////////////////////////////////////////
//// MESSAGE PROCESSING
/////////////////////////////////////////////////////////////////////

function process_message($message) {
    switch($message) {
        case 'register':
            return(process_message_register());
            break;
        default:
            return("Command does not exist '$message'");
            break;
    }
}

function process_message_register($message) {
    echo "Register not implemented\n";
}

/////////////////////////////////////////////////////////////////////
//// END MESSAGE PROCESSING
/////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////
//// Administrative functions
/////////////////////////////////////////////////////////////////////
function load_modules() {
    echo "load_modules() not implemented\n";
}
/////////////////////////////////////////////////////////////////////
//// END Administrative functions
/////////////////////////////////////////////////////////////////////

