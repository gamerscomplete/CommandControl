// BORN TO RAGE

package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"math/rand"
	"net"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
	"unsafe"

	// Inter-process communication
	csTools "gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	// Inter-cluster communication
	"gitlab.com/gamerscomplete/CommandControl/plexus"
	// Generalized toolset
	"github.com/hashicorp/yamux"
	"gitlab.com/gamerscomplete/CommandControl/tools"
	"gitlab.com/gamerscomplete/uuid"
)

//Localizing this to wrap functions to it. This is probably not what I want anymore
type ServerMessage csTools.ServerMessage

type Service struct {
	name      string
	startTime time.Time
	authToken string
	Conn      net.Conn
	process   *exec.Cmd
}

var (
	// Configs
	//plexusPort          = 25007
	genesisSockLocation = "/tmp"
	_log_level          = 0

	// General globals
	plexusConn = plexus.Plexus{}
	//map[service name]service info
	servicesRunning = make(map[string]Service)
	runningLock     sync.Mutex
	servicesPending = make(map[string]chan net.Conn)
	pendingLock     sync.Mutex
	sentSize        int64
	recievedSize    int64
)

type logWriter struct {
	serviceName string
}

const (
	SERVICE_START_TIMEOUT = time.Second * time.Duration(5)
)

func main() {
	log(0, "Launching CommandControl...")
	log(0, "Installing signal handler...")
	installSignalHandler()
	log(0, "Loading arguments...")

	var plexusPort int
	var serviceArgs = flag.String("service", "", "Services to start on invocation")
	var seedURL = flag.String("seed", "", "Seed addr")
	var seedPort = flag.Int("sport", 25007, "Seed port")
	flag.IntVar(&_log_level, "log_level", _log_level, "Log level")
	flag.IntVar(&plexusPort, "port", 25007, "Plexus port")

	flag.Parse()

	plexus.SetLogLevel(_log_level)

	go launchServiceSocket()

	if err := plexusConn.Init(processServerMessage, plexusPort); err != nil {
		log(5, "Failed to init plexus. Error: ", err)
		os.Exit(1)
	}

	if *seedURL != "" {
		if _, err := plexusConn.PeerConnect(*seedURL, *seedPort); err != nil {
			log(3, "Failed to connect to seeded peer")
		}
	}

	// Start services
	if *serviceArgs != "" {
		var services = strings.Split(*serviceArgs, ",")
		for _, element := range services {
			if err := startService(element); err != nil {
				log(3, "Failed to start service: "+element+" - ", err)
			}
		}
	} else {
		log(1, "No services provided to start")
	}

	manageServices()
}

//Bit of an ugly shitshow, but works around logrus putting multiple log entries into one write
func (writer logWriter) Write(data []byte) (int, error) {
	header := []byte("(" + writer.serviceName + ") ")
	var err error
	for _, entry := range strings.Split(string(data), "\n") {
		if len(entry) == 0 {
			continue
		}
		_, err = os.Stderr.Write(header)
		if err != nil {
			break
		}
		_, err = os.Stderr.Write([]byte(entry))
		if err != nil {
			break
		}
		_, err = os.Stderr.Write([]byte("\n"))
		if err != nil {
			break
		}
	}
	return len(data), err
}

func manageServices() {
	//watch for services exiting. Maybe restart services that die and a shutdown signal was not sent
	// Keep the application alive. This is about the worst solution I could come up with
	for {
		time.Sleep(3000 * time.Millisecond)
	}
}

//////////////////////////////////////////
// Module connection handling
//////////////////////////////////////////

// Listening for service communication
func launchServiceSocket() {
	sockDir := "/tmp/CommandControl/"
	if _, err := os.Stat(sockDir); err != nil {
		if os.IsNotExist(err) {
			log(1, "Sock directory does not exist. Creating it at:"+sockDir)
			os.Mkdir(sockDir, 0700)
		}
	}
	// leftovers that should be fixed up
	rawSockLocation := sockDir + "cc.raw.sock"
	if tools.FileExists(rawSockLocation) == true {
		log(1, "Cleaning up old cc.raw.sock")
		if err := os.Remove(rawSockLocation); err != nil {
			log(3, "Could not remove cc.raw.sock file")
		}
	}
	streamSockLocation := sockDir + "cc.stream.sock"
	if tools.FileExists(streamSockLocation) == true {
		log(1, "Cleaning up old cc.stream.sock")
		if err := os.Remove(streamSockLocation); err != nil {
			log(3, "Could not remove cc.stream.sock file")
		}
	}

	rawListenSock, err := net.Listen("unix", rawSockLocation)
	if err != nil {
		log(5, "listen error: ", err)
	}

	streamListenSock, err := net.Listen("unix", streamSockLocation)
	if err != nil {
		log(5, "listen error: ", err)
	}

	go func(rawListenSocket net.Listener) {
		for {
			conn, err := rawListenSocket.Accept()
			if err != nil {
				log(5, "raw socket accept error:", err)
			}
			go handleRawConnection(conn)
		}
	}(rawListenSock)

	go func(streamListenSocket net.Listener) {
		for {
			conn, err := streamListenSock.Accept()
			if err != nil {
				log(5, "stream socket accept error:", err)
			}
			go handleStreamConnection(conn)
		}
	}(streamListenSock)
}

// A module has connected and initiated server connection. Spin off into a new goroutine to service this module
func handleRawConnection(conn net.Conn) {

	// We should probably know who we are at this point so we dont have to track everything by socket
	dec := json.NewDecoder(bufio.NewReader(conn))
	for {
		var serverMessage csTools.ServerMessage
		if err := dec.Decode(&serverMessage); err == io.EOF {
			//Need to list who it was that left
			log(1, "Client disconected. Removing service")
			removeService(conn)
			break
		} else if err != nil {
			log(3, err)
			break
		}
		recievedSize += int64(unsafe.Sizeof(serverMessage))
		serverMessage.FromID = plexusConn.LocalUUID
		go processServerMessage(serverMessage, conn)
	}
}

func handleStreamConnection(conn net.Conn) {
	log(0, "Initiating muxer session")
	session, err := yamux.Server(conn, nil)
	if err != nil {
		log(3, err)
		return
	}
	// TODO: Add session manager listening for new session requests
	log(0, "Accepting controlStream")
	controlStream, err := session.Accept()
	if err != nil {
		log(3, err)
		return
	}

	handleRawConnection(controlStream)
}

func removeService(conn net.Conn) {
	log(1, "Remove service called")
	for key, value := range servicesRunning {
		if conn == value.Conn {
			log(1, "Service shutdown:", servicesRunning[key].name)
			runningLock.Lock()
			delete(servicesRunning, key)
			runningLock.Unlock()
			updatePlexusServices()
		}
	}
}

func updatePlexusServices() {
	var plexusList []string
	//	i := 0
	for serviceName, service := range servicesRunning {
		if serviceName == "" {
			log(1, "someone polluted the service map...")
		}
		//only want services that have completed authentication
		if service.Conn != nil {
			plexusList = append(plexusList, serviceName)
		} else {
			log(1, "Service not authenticated yet", serviceName)
		}
	}
	if len(plexusList) == 0 {
		log(3, "No services being sent.")
	}
	plexusConn.UpdateServices(plexusList)
}

func authenticateService(message csTools.Message, conn net.Conn) bool {
	log(0, "Processing service authentication for token:", message.Parameters)
	for key, value := range servicesPending {
		if key == message.Parameters {
			log(0, "Found authtoken in map. Sending conn back down chan")
			value <- conn
			return true
		}
	}
	log(3, "Did not find service in pending for token:", message.Parameters)
	return false
}

func sendServiceMessage(service string, message ServerMessage) error {
	// Lookup service to make sure it exists in map

	_, found := servicesRunning[service]
	if found {
		//proceed with sending message
		marshaledMessage, err := json.Marshal(message)
		if err != nil {
			log(3, "Failed to marshal message")
			return err
		}

		if servicesRunning[service].Conn == nil {
			return errors.New("Service conn is nil")
		}
		sizeWritten, err := servicesRunning[service].Conn.Write([]byte(marshaledMessage))
		if err != nil {
			log(5, "Socket write failed: ", err)
			return err
		}
		sentSize += int64(sizeWritten)

		return nil
	}
	return errors.New("Service not running")
}

func setLogLevel(level string) {
	//		plexus.SetLogLevel(strconv.Atoi(message.Data.Parameters))
	if logLevel, err := strconv.Atoi(level); err != nil {
		log(3, "Failed to convert log level to integer. String provided '", level, "' ERR:", err)
	} else {
		_log_level = logLevel
		plexus.SetLogLevel(logLevel)
	}
}

//should come in a different flow between plexus and services
func processServerMessage(incomingMessage csTools.ServerMessage, conn net.Conn) {
	var message = ServerMessage(incomingMessage)
	if message.ToID != plexusConn.LocalUUID {
		if message.FromID != plexusConn.LocalUUID {
			log(3, "Attempting to send a forged address: ", message.FromID.String())
			return
		}
		message.FromID = plexusConn.LocalUUID
		log(0, "Forwarding message via plexus to:", message.ToID.String())
		plexusConn.SendMessage(incomingMessage)
		return
	}

	if message.ToService != "genesis" && message.ToService != "" {
		log(0, "Routing message to service :", message.ToService)
		if err := sendServiceMessage(message.ToService, message); err != nil {
			//		if err := sendServiceMessage(unwrapped.ToService, unwrapped); err != nil {
			log(2, "Failed to send message to service:", message.ToService, " From: ", message.FromService, "With error:", err)
		}
		return
	}

	switch message.Data.Command {
	case "NEW_NETWORK":
		plexusConn.CreateNetwork(message.Data.Parameters)
	case "PLEXUS_CONNECT":
		go plexusConnect(message, conn)
	case "START_SERVICE":
		go startServiceRequest(message)
	case "STOP_SERVICE":
		go stopServiceRequest(message)
	case "SERVICE_NODES_BY_NAME":
		go processNodesByName(message)
	case "GET_SERVICES":
		go getServices(message)
		//list services
	case "SET_LOG_LEVEL":
		setLogLevel(message.Data.Parameters)
	case "AUTH":
		//TODO: Wrap in function
		if authenticateService(message.Data, conn) {
			sendMessageConn(conn, ServerMessage{Time: tools.EpochTime(), JobID: message.JobID, IsReply: true, Data: csTools.Message{Command: "AUTH", Parameters: "SUCCESS"}})
		} else {
			log(2, "Invalid auth token: ", message.Data.Parameters)
			sendMessageConn(conn, ServerMessage{Time: tools.EpochTime(), JobID: message.JobID, IsReply: true, Data: csTools.Message{Command: "AUTH", Parameters: "INVALID TOKEN"}})
			//authfailed
		}
	case "STATUS":
		message.reply(csTools.Message{Command: "ACK"})
	case "GET_UUID":
		message.reply(csTools.Message{Command: "UUID", Parameters: plexusConn.LocalUUID.String()})
	case "ESTABLISH_PIPELINE":
		log(3, "ESTABLISH_PIPELINE not implemeneted")
		//{from_service:to_service:to_peer}
		//		establishPipeline(message.Data.Parameters)
	case "PING":
		log(1, "Ponging back to:", message.FromID.String(), " / ", message.FromService)

		message.reply(csTools.Message{
			Command:    "PONG",
			Parameters: plexusConn.LocalUUID.String(),
		})
	case "PLEXUS_STATUS":
		go plexusStatus(message, conn)
	case "SHOW_ROUTE":
		log(1, "Generating route text")
		parsedUUID, err := uuid.ParseHex(message.Data.Parameters)
		if err != nil {
			log(2, "Failed to parse uuid from show_route command")
			return
		}

		var returnMessage string
		returnMessage = "Route length: " + strconv.Itoa(len(plexusConn.RouteMapCache[*parsedUUID].Route)) + " | Total Cost: " + strconv.FormatInt(plexusConn.RouteMapCache[*parsedUUID].Cost, 10) + "\n"
		for i := 0; i < len(plexusConn.RouteMapCache[*parsedUUID].Route); i++ {
			id := plexusConn.RouteMapCache[*parsedUUID].Route[i]
			node := plexusConn.RouteMapCache[id]
			returnMessage = returnMessage + "HOP: " + strconv.Itoa(i) + "\tCost: " + strconv.FormatInt(node.Cost, 10) + "\tID: " + id.String() + "\n"
		}

		message.reply(csTools.Message{Command: "ROUTE", Parameters: returnMessage})
	default:
		log(3, "Unknown server command recieved", message.Data.Command)
	}
}

func (message ServerMessage) reply(newMessage csTools.Message) {
	log(0, "Replying to job:", message.JobID)
	if message.FromID == plexusConn.LocalUUID || message.FromID == *new(uuid.UUID) {
		log(1, "(reply)ToID:", message.ToID.String(), "Local UUID:", plexusConn.LocalUUID.String())
		log(1, "(reply)Delivering message locally to service:", message.FromService)
		if _, ok := servicesRunning[message.FromService]; ok {
			replyMessage := ServerMessage{
				ToID:        message.FromID,
				ToService:   message.FromService,
				FromID:      plexusConn.LocalUUID,
				FromService: "genesis",
				IsReply:     true,
				JobID:       message.JobID,
				Data:        newMessage,
			}
			//This seems super fucktacular. It is using the message
			sendMessageConn(servicesRunning[message.FromService].Conn, replyMessage)
		} else {
			log(2, "Attempted to deliver message to service that does not exist. Service: ", message.FromService)
		}
	} else {
		log(1, "Sending message to:", message.FromID.String())
		//TODO: Implement actual peer checking. Syncing is not working right now so just fire it regardless
		if true {
			var serverMessage = csTools.ServerMessage{
				ToID:        message.FromID,
				ToService:   message.FromService,
				FromID:      plexusConn.LocalUUID,
				FromService: "genesis",
				IsReply:     true,
				JobID:       message.JobID,
				Data:        newMessage,
			}
			log(0, "Sending external message")
			if err := plexusConn.SendMessage(serverMessage); err != nil {
				log(2, "Failed to send message")
			} else {
				log(0, "Message sent")
			}
		} else {
			log(2, "Attempted to send message to non existant address")
			return
		}
	}
}

func plexusConnect(message ServerMessage, conn net.Conn) {
	//	start := time.Now()
	log(1, "Connecting to peer at:", message.Data.Parameters, ":25007")
	addrParts := strings.Split(message.Data.Parameters, ":")

	//TODO: this is pretty shit and needs to be handled better

	connPort, err := strconv.Atoi(addrParts[1])
	replyMessage := csTools.Message{Command: "REPLY"}
	if err != nil {
		replyMessage.Error = "invalid port"
	} else if len(addrParts) != 2 {
		replyMessage.Error = "Invalid connection string"
		log(3, replyMessage)
	} else {
		peerID, err := plexusConn.PeerConnect(addrParts[0], connPort)
		if err != nil {
			replyMessage.Error = "Failed to connect to peer"
			log(3, err)
		} else {
			if err := replyMessage.PackType(&peerID); err != nil {
				log(3, "Failed to pack reply to plexusConnect", err)
				replyMessage.Error = "Failed to pack reply"
			}
		}
	}

	sendMessageConn(conn, ServerMessage{
		Data:    replyMessage,
		Time:    tools.EpochTime(),
		IsReply: true,
		JobID:   message.JobID,
	})
}

func getServices(message ServerMessage) {
	replyMessage := csTools.Message{Command: "REPLY"}
	replyMessage.PackType(csTools.ClusterServiceMap{Services: plexusConn.ServiceMapCache})
	message.reply(replyMessage)
}

func plexusStatus(message ServerMessage, conn net.Conn) {
	//	start := time.Now()
	//PLEXUS_STATUS is a client only command and needs to be protected from peers or, change to send reply to whomever requested

	newMessage := "Network Name: " + plexusConn.NetworkEnvelope.Network.Name +
		" Version: " + plexusConn.NetworkEnvelope.Network.Version.String() +
		" Global peers: " + strconv.Itoa(len(plexusConn.NetworkEnvelope.Network.Peers)) +
		" Local peers: " + strconv.Itoa(len(plexusConn.LocalPeerMap)) +
		"\n"

	i := 0
	if message.Data.Parameters == "global" {
		for key, value := range plexusConn.NetworkEnvelope.Network.Peers {
			i++
			local := ""
			//			if _, found := plexusConn.LocalPeerMap[value.UUID]
			if len(plexusConn.NetworkEnvelope.Network.Peers) != i {
				newMessage += local + "UUID: " + key.String() + " Version: " + value.Version.String() + "\n"
			} else {
				newMessage += "UUID: " + key.String() + " Version: " + value.Version.String()
			}
		}
	} else {
		for key, value := range plexusConn.LocalPeerMap {
			i++
			if len(plexusConn.LocalPeerMap) != i {
				newMessage += "UUID: " + key.String() + " address: " + value.Address + "\n"
			} else {
				newMessage += "UUID: " + key.String() + " address: " + value.Address
			}
		}
	}
	sendMessageConn(conn, ServerMessage{
		Data: csTools.Message{
			Command:    "Reply",
			Parameters: newMessage,
		},
		Time:    tools.EpochTime(),
		IsReply: true,
		JobID:   message.JobID,
	})
}

//Be pretty cool if this actually worked. But don't have a good enough use case to warrant the time investment yet
/* func establishPipeline(params string) {
	//The processing of the command should be handled by another function. well do that later
	//Validate input parameters
	//{from_service:to_service}
    requestSplit := strings.Fields(params)
	if len(requestSplit) != 2 {
		log(3, "Invalid pipeline request. Expected 2 arguements. Recieved: ", string(len(requestSplit)))
		return
	}

    fromService := strings.Split(requestSplit[0], ":")
    toService := strings.Split(requestSplit[1], ":")

	//Validate the from service is valid
	//validate the to service is valid
	//

}
*/
func sendMessageConn(socket net.Conn, message ServerMessage) {
	log(0, "sendMessageConn sending message to peer")
	marshaledMessage, err := json.Marshal(message)
	if err != nil {
		log(3, "Failed to marshal message")
		return
	}

	_, err = socket.Write([]byte(marshaledMessage))
	if err != nil {
		log(5, "Socket write failed: ", err)
		return
	}
	sentSize += int64(unsafe.Sizeof(marshaledMessage))
}

func sendMessageUUID(toID uuid.UUID, message ServerMessage) {
	if plexusConn.LocalUUID == toID {
		log(1, "(sendMessageUUID)Delivering message locally to service:", message.ToService)
		if _, ok := servicesRunning[message.ToService]; ok {
			sendMessageConn(servicesRunning[message.ToService].Conn, message)
		} else {
			log(2, "Attempted to deliver message to service that does not exist. Service: ", message.ToService)
		}
	} else {
		//TODO: Implement actual peer checking. Syncing is not working right now so just fire it regardless
		if true {
			//Exists. Send it
			message.FromID = plexusConn.LocalUUID
			plexusConn.SendMessage(csTools.ServerMessage(message))

			log(1, "Sending message to:", toID.String())
			//deliver remotely
		} else {
			log(2, "Attempted to send message to non existant address")
			return
		}
	}
}

//////////////////////////////////////////
// Service handling
//////////////////////////////////////////

func checkServiceRunning(serviceName string) bool {
	if _, found := servicesRunning[serviceName]; found {
		return true
	}
	return false
}

func stopService(serviceName string) error {
	//This should handle removing from the service list and then updating
	servicesRunning[serviceName].process.Process.Kill()
	runningLock.Lock()
	delete(servicesRunning, serviceName)
	runningLock.Unlock()
	updatePlexusServices()
	return nil
}

func createAuthToken() string {
	rand.Seed(time.Now().UnixNano())
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	authToken := make([]rune, 10)
	for i := range authToken {
		authToken[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(authToken)
}

func processNodesByName(message ServerMessage) {
	var serviceName string
	if err := message.Data.UnpackType(&serviceName); err != nil {
		message.reply(csTools.Message{Error: "Failed to unpack request"})
		return
	}

	if serviceName == "" {
		log(1, "Empty service name in process nodes by name")
	}

	var returnList []uuid.UUID

	//Get external services
	if serviceList, found := plexusConn.ServiceMapCache[serviceName]; found {
		for _, value := range serviceList {
			returnList = append(returnList, value)
		}
	}

	//servicemapcache does not have local services, so check if we have one locally and append it as well
	if _, found := servicesRunning[serviceName]; found {
		returnList = append(returnList, plexusConn.LocalUUID)
	}

	replyMessage := csTools.Message{}
	if err := replyMessage.PackType(returnList); err != nil {
		message.reply(csTools.Message{Error: "Failed to pack reply"})
		return
	}

	message.reply(replyMessage)
	return
}

func startServiceRequest(message ServerMessage) {
	var serviceName string
	if err := message.Data.UnpackType(&serviceName); err != nil {
		message.reply(csTools.Message{Error: "Failed to unpack request"})
		return
	}

	if err := startService(serviceName); err != nil {
		message.reply(csTools.Message{Error: err.Error()})
		return
	}

	replyMessage := csTools.Message{}
	if err := replyMessage.PackType("SUCCESS"); err != nil {
		log(3, "Failed to pack reply", err)
		replyMessage.Error = "Failed to pack reply"
		message.reply(replyMessage)
		return
	}

	message.reply(replyMessage)
}

func stopServiceRequest(message ServerMessage) {
	var serviceName string
	if err := message.Data.UnpackType(&serviceName); err != nil {
		message.reply(csTools.Message{Error: "Failed to unpack request"})
		return
	}

	if err := stopService(serviceName); err != nil {
		message.reply(csTools.Message{Error: "Failed to unpack request"})
		return
	}

	message.reply(csTools.Message{Parameters: "SUCCESS"})
}

func startService(serviceName string) error {
	//Check service running
	if checkServiceRunning(serviceName) {
		errMsg := "Service requested to be started already running:" + serviceName
		log(2, errMsg)
		return errors.New(errMsg)
	}
	log(1, "Starting service", serviceName)

	servicePath := "modules/" + serviceName + "/"
	cmdPath := servicePath + serviceName

	if !tools.FileExists(cmdPath) {
		errMsg := "Service cmd could not be found at " + cmdPath
		log(3, errMsg)
		return errors.New(errMsg)
	}

	cwd, err := os.Getwd()
	if err != nil {
		log(3, "Failed to get working direction:", err)
	}

	cmd := exec.Command(cwd + "/" + servicePath + serviceName)
	env := os.Environ()

	authToken := createAuthToken()

	env = append(env, fmt.Sprintf("AUTH_TOKEN=%s", authToken))
	env = append(env, fmt.Sprintf("ID=%s", plexusConn.LocalUUID))

	cmd.Env = env

	cmd.Dir = cwd + "/" + servicePath

	serviceConfig, err := parseServiceConfig(servicePath)
	if err != nil {
		log(3, err)
	}

	if _, ok := serviceConfig["stdin"]; ok {
		if serviceConfig["stdin"] == "true" {
			log(0, "Service configuration requested stdin")
			cmd.Stdin = os.Stdin
		}
	}
	if _, ok := serviceConfig["stdout"]; ok {
		if serviceConfig["stdout"] == "true" {
			log(0, "Service configuration requested stdout")
			cmd.Stdout = os.Stdout
		}
	}
	if _, ok := serviceConfig["stderr"]; ok {
		if serviceConfig["stderr"] == "true" {
			log(0, "Service configuration requested stderr")
			cmd.Stderr = logWriter{serviceName: serviceName}
		}
	}

	//TODO: Capture stderr here and pipe into logging aggregator

	/*
	 * Cgroup splice
	 */

	if err := cmd.Start(); err != nil {
		fmt.Println("DEBUG DUMP:", cwd+"/"+servicePath+serviceName, cmd)
		return err
	}

	startChan := make(chan net.Conn)
	pendingLock.Lock()
	servicesPending[authToken] = startChan
	log(0, "Starting service with authtoken:", authToken)
	pendingLock.Unlock()

	//Timeout on starting service
	select {
	case serviceConn := <-startChan:
		log(1, "Serviced authenticated:", serviceName)
		service := Service{name: serviceName, startTime: time.Now(), authToken: string(authToken), process: cmd, Conn: serviceConn}
		pendingLock.Lock()
		runningLock.Lock()
		delete(servicesPending, authToken)
		servicesRunning[serviceName] = service
		pendingLock.Unlock()
		runningLock.Unlock()
		updatePlexusServices()
		log(1, "Service started:", serviceName)
	case <-time.After(SERVICE_START_TIMEOUT):
		return errors.New("Timed out")
	}

	return nil
}

func parseServiceConfig(path string) (map[string]string, error) {
	var serviceConfig = make(map[string]string)

	//Check if a configuration file exists for the service
	if tools.FileExists(path + "service.cfg") {
		log(0, "Service configuration file found. Loading...")

		serviceConfigFile, err := os.Open(path + "service.cfg")
		if err != nil {
			return nil, errors.New("Failed to open service configuration file")
		}
		defer serviceConfigFile.Close()

		scanner := bufio.NewScanner(serviceConfigFile)
		for scanner.Scan() {
			//Get new line from config file
			configLineText := scanner.Text()
			log(0, "Read configuration line:", configLineText)
			configOption := strings.Split(configLineText, "=")
			if len(configOption) != 2 {
				if configLineText != "" {
					log(2, "Failed to parse configuration option:", configLineText)
				}
			} else {
				log(0, "Adding configuration to map:", configOption[0], "=", configOption[1])
				serviceConfig[configOption[0]] = configOption[1]
				//add configuration to config map
			}
		}

		if err := scanner.Err(); err != nil {
			//			log(3, "Scanner failed reading configuration file with error:", err)
			return nil, err
		}
	}
	return serviceConfig, nil
}

//////////////////////////////////////////
// UTILS
//////////////////////////////////////////
func log(level int, message ...interface{}) {
	if level >= _log_level {
		fmt.Print("Genesis:(", tools.GetCallerFunction(), ") - ", message, "\n")
	}
}

type logMessage struct {
	level          int
	message        int
	sendingService int
	sendingPeer    string
}

func installSignalHandler() {
	// Handle common process-killing signals so we can gracefully shut down:
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		os.Interrupt,
		os.Kill,
		syscall.SIGTERM,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGQUIT,
	)
	go func(c chan os.Signal) {
		// Wait for a SIGINT or SIGKILL:
		sig := <-c
		log(0, "Caught signal ", sig, ": ought to do something with it, but we wont yet")
		//		serverShutdown()
		os.Exit(0)
	}(sigc)
}
