set -e
echo "Building genesis"
cd genesis && CGO_ENABLED=0 go build -o genesis genesis.go
cd -
echo "Building CLI"
cd modules/cli && CGO_ENABLED=0 go build -o cli *.go
cd -
echo "Building proxasaurus"
cd modules/proxasaurus && CGO_ENABLED=0 go build -o proxasaurus proxasaurus.go
cd ~/checkout/src/gitlab.com/gamerscomplete/CommandControl/
#TAG=gitlab.com/gamerscomplete/commandcontrol:$(date +%s)
#echo "Building docker container"
#docker build -t $TAG -t gitlab.com/gamerscomplete/commandcontrol:latest . >/dev/null
#docker push $TAG >/dev/null
