package types

//import ("time")

type CliCommand struct {
	Command     string
	Description string
	Example     string
}

type ServiceMessage struct {
	ServiceName string
	Destination string
	Payload     interface{}
}
