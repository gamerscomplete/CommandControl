#!/bin/bash

mkdir -p .temp_package/CommandControl/modules/cli

cp genesis .temp_package/CommandControl/genesis
cp modules/cli/cli .temp_package/CommandControl/modules/cli/cli
cp .aliasDB.gob .temp_package/CommandControl/.aliasDB.gob

cd .temp_package
tar -cjf CommandControl.tar.bz2 CommandControl
mv CommandControl.tar.bz2 ../CommandControl.tar.bz2
cd ../
rm -rf .temp_package
rsync -a CommandControl.tar.bz2 web-001:/var/www/html/
