package plexus

import (
	"errors"
)

func processInternalMessage(pmessage PeerMessage) {
	internalMessage, found := pmessage.Payload.(*InternalMessage)
	if !found {
		log(3, "Failed to assert InternalMessage type")
	}
	switch internalMessage.Type {
	case 1:
		//Request peer addition
		success := leaderRequestPeerAddition(*internalMessage)
		if success {
			//          replyMessage := InternalMessage{Approved: true}
			//reply back with gud
		} else {
			//          replyMessage := InternalMessage{Approved: false}
			//reply back with the bad
		}
	case 2:
		//Update
	case 3:
		//Vote?
	default:
		log(3, "Recieved invalid internal message type", internalMessage.Type)
	}
}
func leaderRequestPeerAddition(internalMessage InternalMessage) bool {
	return false
}

func (plexus *Plexus) sendTwoWayMessage(message PeerMessage) (chan InternalMessage, error) {
	//generate job id
	//generate a channel
	returnChan := new(chan InternalMessage)
	//send message down the line
	nextHop, err := plexus.getRouteHop(message.To)
	if err != nil {
		return *returnChan, err
	}
	plexus.LocalPeerMap[nextHop].SendQueue <- message
	//return channel
	return *returnChan, nil
}

type messageQueue struct {
	jobs map[int]chan InternalMessage
}

func (queue *messageQueue) add() {
	log(3, "Not implemented")
}

func (message *PeerMessage) reply(internalMessage InternalMessage, plexus *Plexus) error {
	var plexusMessage = PeerMessage{
		From:        message.To,
		To:          message.From,
		MessageType: 2,
		Payload:     internalMessage,
	}
	//plexus not available here. need to find some more effect way of peer exists
	if !plexus.NetworkEnvelope.Network.peerExists(message.From) {
		log(3, "Peer didn't exist in map. Peer:", message.From)
		return errors.New("shit busted")
	}
	nextHop, err := plexus.getRouteHop(message.To)
	if err != nil {
		return err
	}

	plexus.LocalPeerMap[nextHop].SendQueue <- plexusMessage
	return nil
}
