package plexus

import (
	"errors"
	"gitlab.com/gamerscomplete/CommandControl/clientServerTools"
)

func (plexus *Plexus) SendMessage(serverMessage clientServerTools.ServerMessage) (err error) {
	if !plexus.NetworkEnvelope.Network.peerExists(serverMessage.ToID) {
		log(3, "Peer didnt exist in map. Peer:", serverMessage.ToID.String())
		return errors.New("Attempted to send message to peer not in map")
	}

	var plexusMessage = PeerMessage{
		From:        plexus.LocalUUID,
		To:          serverMessage.ToID,
		MessageType: 3,
		Payload:     serverMessage,
	}

	log(0, "Sending plexusmessage")
	/*  if _, success := plexus.LocalPeerMap[serverMessage.ToID]; !success {
	    log(3, "Peer existed in global but not local:", serverMessage.ToID.String())
	    return errors.New("Failed to send message")
	}*/
	nextHop, err := plexus.getRouteHop(serverMessage.ToID)
	if err != nil {
		log(3, "Failed to find next hop:", err)
		return err
	}
	log(0, "NextHop:", nextHop.String())
	plexus.LocalPeerMap[nextHop].SendQueue <- plexusMessage
	return nil
}
