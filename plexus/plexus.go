// 300 POUNDS OF BAD MOTHERFUCKER

package plexus

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/gob"
	"errors"
	"fmt"
	"io"
	"net"
	"strconv"
	"time"

	"github.com/hashicorp/yamux"
	"gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/CommandControl/tools"
	"gitlab.com/gamerscomplete/uuid"
)

const (
	_syncMinTime = 1
	_syncMaxTime = 5
)

var logLevel int

func SetLogLevel(level int) {
	log(0, "Changing log level from", logLevel, "To:", level)
	logLevel = level
}

// Setup of initial launch
func (plexus *Plexus) Init(callback fn, listenPort int) (err error) {
	id, err := uuid.NewV4()
	if err != nil {
		log(4, "Unable to generate a UUID")
		return errors.New("Unable to generate a UUID")
	}

	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		log(4, "Failed to generate private key:", err)
		return err
	}

	*plexus = Plexus{
		LocalUUID:    *id,
		LocalPeerMap: make(map[uuid.UUID]*localPeer),
		callback:     callback,
		privateKey:   *privateKey,
		Mode:         0,
	}

	go plexus.startRecieveSocket(listenPort)

	return nil
}

// Startup a listening socket
func (plexus *Plexus) startRecieveSocket(port int) {
	l, err := net.Listen("tcp", ":"+strconv.Itoa(port))
	if err != nil {
		log(5, "listen error: ", err)
	}

	log(1, "Plexus listening on socket:", port)

	for {
		conn, err := l.Accept()
		if err != nil {
			log(4, "accept error:", err)
		}
		log(0, "Recieved a new connection")
		go plexus.handleIncomingConnection(conn)
	}
}

//////////////////////////////////
// Peer connection handling
//////////////////////////////////

func (plexus *Plexus) handleIncomingConnection(conn net.Conn) {
	log(0, "Initiating muxer session")
	session, err := yamux.Server(conn, nil)
	if err != nil {
		log(3, err)
		return
	}
	// TODO: Add session manager listening for new session requests
	log(0, "Accepting controlStream")
	controlStream, err := session.Accept()
	if err != nil {
		log(3, err)
		return
	}

	decoder := gob.NewDecoder(controlStream)
	encoder := gob.NewEncoder(controlStream)

	gob.Register(&clientServerTools.ServerMessage{})
	gob.Register(&clientServerTools.Message{})
	gob.Register(&AuthPacket{})

	//Initial packet with just uuid and time to get latency
	var authPacket AuthPacket
	log(0, "Waiting for initial authpacket")
	if err = decoder.Decode(&authPacket); err != nil {
		log(2, "Failed to decode authPacket")
		return
	}
	log(0, "Recieved initial authpacket from:", authPacket.UUID.String())

	//Fire back the same type of packet so the peer knows our uuid
	authReply := AuthPacket{Time: time.Now(), UUID: plexus.LocalUUID}

	log(0, "Sending initial auth packet reply")
	if err := encoder.Encode(&authReply); err != nil {
		log(2, "Failed to encode initial handshake reply", err)
		return
	}

	log(0, "Waiting for full authpacket")
	if err = decoder.Decode(&authPacket); err != nil {
		log(2, "Failed to decode authPacket")
		return
	}

	if authPacket.GlobalPeer.UUID == *new(uuid.UUID) {
		log(3, "Recieved zero UUID")
		return
	}

	log(0, "Processing peering request for", authPacket.GlobalPeer.UUID.String())
	success, authReply := plexus.processPeeringRequest(authPacket)
	if !success {
		log(2, "Peer failed authentication")
		return
	}
	authReply.UUID = plexus.LocalUUID

	if err := encoder.Encode(&authReply); err != nil {
		log(2, "Failed to encode handshake reply", err)
		return
	}

	////OUGHT NOT BE REPLYING YET. Leader has not approved of this addition

	log(0, "Accepting syncStream")
	syncStream, err := session.Accept()
	if err != nil {
		log(3, "Failed to create a new stream for syncing")
		return
	}
	log(0, "Stream accepted")

	sendQueue := make(chan PeerMessage, 100)
	newPeer := localPeer{UUID: authPacket.UUID, Address: conn.RemoteAddr().String(), SendQueue: sendQueue, StreamSession: *session}

	//Add the peer to the local connection map
	log(0, "Adding to localpeermap", authPacket.UUID.String())
	plexus.LocalPeerMap[authPacket.UUID] = &newPeer

	go plexus.peerSyncer(syncStream)
	go plexus.handleDecoder(decoder)
	go handleEncoder(encoder, sendQueue)
}

func (plexus *Plexus) requestPeerAddition(globalPeer GlobalPeer) error {
	if plexus.NetworkEnvelope.Network.Leader == plexus.LocalUUID {
		if err := plexus.addPeerToNetwork(globalPeer); err != nil {
			return err
		}
		return nil
	}
	return errors.New("Requested unimplemeneted feature")
	//TODO: Send a 2way request to the leader for addition request
}

func handleEncoder(encoder *gob.Encoder, sendQueue chan PeerMessage) {
	for {
		newMessage := <-sendQueue
		log(0, "Encoding message to:", newMessage.To.String())
		log(0, "Message to encode: ", newMessage)
		if err := encoder.Encode(&newMessage); err != nil {
			log(3, "Failed to encode message in SendMessage", err)
		}
		log(0, "Message encoded")
	}
}

func (plexus *Plexus) handleDecoder(decoder *gob.Decoder) {
	var pmessage PeerMessage
	for {
		log(0, "waiting for peer message")
		if err := decoder.Decode(&pmessage); err == io.EOF {
			//Terminate connection
			log(1, "Client hung up on us bye bye")
			break
		} else if err != nil {
			log(4, "Error decoding message", err)
			log(1, "Message: ", pmessage)
			//			log(1, "serverMessage: ", *pmessage.Payload.(*clientServerTools.ServerMessage))
			continue
		}
		//		log(0, "Message: ", pmessage)
		log(0, "Recieved peer message from ", pmessage.From.String(), "To:", pmessage.To.String())
		//		dereferenced := pmessage.Payload.(*clientServerTools.ServerMessage)
		//		log(0, "serverMessage: ", *dereferenced)
		plexus.processPeerMessage(pmessage)
	}
}

func (plexus *Plexus) NewStream(streamPeer *localPeer) (net.Conn, error) {
	log(0, "Creating new stream")
	stream, err := streamPeer.StreamSession.Open()
	log(0, "Got stream")
	if err != nil {
		log(4, err)
		return nil, err
	}
	log(0, "Stream succesfully created")
	return stream, nil
}

//////////////////////////////////
/// Message handling
//////////////////////////////////

//Process an incoming message from another plexus instance
func (plexus *Plexus) processPeerMessage(pmessage PeerMessage) {
	//This should be gutted in lieu of side channels for different message types to speed the pipeline
	switch {
	case pmessage.To == plexus.LocalUUID:
		switch pmessage.MessageType {
		case 0:
			log(3, "Recieved invalid peer message")
			return
		case 2:
			//inter plexus communication message
			processInternalMessage(pmessage)
		case 3:
			//deliver
			serverMessage, found := pmessage.Payload.(*clientServerTools.ServerMessage)
			if !found {
				log(3, "Failed to assert ServerMessage type")
			}
			log(0, "deliver message callback")
			plexus.callback(*serverMessage, nil)
		default:
			log(2, "Recieved unknown message type")
		}
	case plexus.NetworkEnvelope.Network.peerExists(pmessage.To):
		id, err := plexus.getRouteHop(pmessage.To)
		if err != nil {
			log(2, "Unable to get next route hop for forwarding message")
		}
		log(1, "Routing message from:", pmessage.From.String(), "To:", pmessage.To.String(), "Via:", id.String())
		plexus.LocalPeerMap[id].SendQueue <- pmessage
	default:
		log(2, "Recieved message for unknown peer:", pmessage.To)

	}
}

//////////////////////////////////
/// Peer utilities
//////////////////////////////////

func (network *Network) peerExists(peer uuid.UUID) bool {
	_, found := network.Peers[peer]
	return found
}

func (plexus *Plexus) removeLink(peer uuid.UUID) {
	log(2, "Removing a peer. This isnt fully implemeneted")
	delete(plexus.LocalPeerMap, peer)
}

/////////////////////////////////
/// General Utilities
/////////////////////////////////

func log(level int, message ...interface{}) {
	if level < logLevel {
		return
	}
	fmt.Print("Plexus:(", tools.GetCallerFunction(), ") - ", message, "\n")
}

//func intToString(integer int) string {
//	return strconv.Itoa(integer)
//}

///////////////////////////////////////////////////////
///              Not implemented yet
///////////////////////////////////////////////////////

func getHostInformation() {
	//Interfaces must be collected first as the route struct has dependencies
	// Collect Interfaces
	// Collect routes
	// Determine external IP
}

/////////////////////////////////////////////
/// Boneyard
/////////////////////////////////////////////
// https://golang.org/pkg/net/#IP
type HostInfo struct {
	Hostname              string
	Interfaces            map[string]string
	SystemUptime          int
	CCUptime              int
	ExternalAddress       string
	StartedBy             string
	UUID                  string
	LastCommunication     int
	RunningAs             string
	ConnectionEstablished int
}

type NetworkInterface struct {
	InterfaceName string
	MTU           int
	Status        string
	Addresses     []NetworkAddress
	Mac           string
}

type NetworkAddress struct {
	IP       string
	Family   string
	CIDR     int
	Netmask  int
	IPFamily string
}

type Route struct {
	Destination string
	Gateway     string
	Mask        string
	Flags       string
	Metric      int
	Interface   NetworkInterface
}

/*
	 * Ideas for ways to determine the correct version to use:

	    1) Newest wins:
			Cons:
				- If its the start of the network where does the first one come from.
				- easily poison whole network
				- Doesn't necesarrily guarantee the correct version
			Pros:
				- Easy to implement

		2) Highest consesus votes: Peers would all vote to accept the new map by verifying their portion is correct
			Cons:
				- Difficult to implement
				- First peer connection
			Pros:
				- Theoretically guarantees correctness

		3) Highest verified peer count: Peers sign that their node is valid, so new additions can be added
			Cons:
				- Needs infrastructure not in place yet (crypto)
				- Difficult to implement
				- Validation could be complicated and error prone
				- Node deletion may provide some logic issues since the generated map would be the correct one, but would have less peers
			Pros:
				- High level of assurance that the outcome is correct
			Hmm:
				- In the event of a cluster split the minority would remerge to the larger cluster


		3 seems to be the winner as of current
*/
//				syncPacket = SyncPacket{Version: plexus.mapVersion, sendTime: time.Now(), GlobalPeerMap: plexus.GlobalPeerMap}
