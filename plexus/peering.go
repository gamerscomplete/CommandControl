package plexus

import (
	"encoding/gob"
	"errors"
	"github.com/hashicorp/yamux"
	"gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/uuid"
	"io"
	"net"
	"strconv"
	"time"
)

// Add a connection to the peer map
func (plexus *Plexus) PeerConnect(ip string, port int) (peerID uuid.UUID, err error) {
	if ip == "" {
		return peerID, errors.New("Invalid address")
	}
	if port < 1 || port > 65535 {
		return peerID, errors.New("Invalid port")
	}

	//This needs error checking to make sure the port converted correctly
	peerConn, err := net.Dial("tcp", ip+":"+strconv.Itoa(port))
	if err != nil {
		log(4, err)
		return peerID, err
	}

	log(0, "Creating client session")
	session, err := yamux.Client(peerConn, nil)
	if err != nil {
		log(4, err)
		return peerID, err

	}

	controlStream, err := session.Open()
	if err != nil {
		log(4, err)
		return peerID, err
	}

	log(1, "Initiating peer connection")

	log(0, "Initiating new encoder on controlStream")
	encoder := gob.NewEncoder(controlStream)
	log(0, "Initiating new decoder on controlStream")
	decoder := gob.NewDecoder(controlStream)

	gob.Register(&clientServerTools.ServerMessage{})

	//Handle peering
	peerUUID, err := plexus.requestNetworkPeering(encoder, decoder)
	if err != nil {
		log(2, "Handshake failed with: ", err)
		return peerID, err
	}

	sendQueue := make(chan PeerMessage, 100)
	//WARNING: This is an issue allowing the client to tell us what the map name will be. needs some fixin
	newPeer := localPeer{UUID: peerUUID, Address: ip, Port: port, SendQueue: sendQueue, StreamSession: *session}
	if err = plexus.addLocalPeer(&newPeer); err != nil {
		return peerID, err
	}

	syncStream, err := plexus.NewStream(&newPeer)
	if err != nil {
		log(4, "Failed to create a new stream for syncing")
		return peerID, err
	}

	log(0, "Starting peer syncer")
	go plexus.peerSyncer(syncStream)
	log(0, "Starting decoder")
	go plexus.handleDecoder(decoder)
	log(0, "Starting encoder")
	go handleEncoder(encoder, sendQueue)

	return peerUUID, nil
}

/*

func (plexus *Plexus) requestLink(uuid string) error {
	return errors.New("requestLink not implemented")
	//	authRequest := AuthPacket{Time: time.Now(), NetworkVersion: plexus.NetworkEnvelope.Network.Version, UUID: plexus.LocalUUID}
}
*/

func (plexus *Plexus) requestNetworkPeering(encoder *gob.Encoder, decoder *gob.Decoder) (uuid.UUID, error) {
	log(0, "Building out network peering request")
	gob.Register(&AuthPacket{})

	//	requestType := ""
	authRequest := AuthPacket{}

	if plexus.Mode != 0 {
		//Send request for peer to join our network
		return *new([16]byte), errors.New("already part of a network")
	}

	//Determine latency, exchange id's
	initialExchange := AuthPacket{Time: time.Now(), UUID: plexus.LocalUUID}

	log(0, "Encoding initial handshake")
	if err := encoder.Encode(&initialExchange); err != nil {
		return *new([16]byte), errors.New("Failed to encode message")
	}

	var authReply AuthPacket
	log(0, "Waiting for initial reply")
	if err := decoder.Decode(&authReply); err == io.EOF {
		log(0, "Client hung up on us before handshake could be completed")
		return *new([16]byte), errors.New("Client hung up before initial handshake could be completed")
	} else if err != nil {
		log(3, "Error decoding message:", err)
		return *new([16]byte), err
	}

	log(0, "Recieved initial reply. UUID:", authReply.UUID.String())
	newPeer := plexus.createGlobalPeer()
	newPeer.ConnMap[authReply.UUID] = time.Since(authReply.Time)

	//Pass our newly built peer in
	authRequest = AuthPacket{Time: time.Now(), GlobalPeer: newPeer, UUID: plexus.LocalUUID}
	log(0, "globalpeerid:", authRequest.GlobalPeer.UUID.String())

	log(0, "Encoding handshake")
	if err := encoder.Encode(&authRequest); err != nil {
		return *new([16]byte), errors.New("Failed to encode message")
	}

	//Got response from the peer, lets see what we got

	log(0, "Waiting for reply")
	if err := decoder.Decode(&authReply); err == io.EOF {
		log(0, "Client hung up on us before handshake could be completed")
		return *new([16]byte), errors.New("Client hung up before handshake could be completed")
	} else if err != nil {
		log(3, "Error decoding message:", err)
		return *new([16]byte), err
	}

	log(0, "Auth reply recieved")

	//!!!Process reply
	if authReply.Response != "ACCEPTED" {
		log(1, "Handshake failed with invalid AuthString: ", authReply.Response)
		return *new([16]byte), errors.New("Handshake failed")
	}

	//Expect to recieve a network from the peer
	if authReply.NetworkEnvelope.Network.Name != "" {
		plexus.updateNetwork(authReply.NetworkEnvelope)
		plexus.Mode = 2
		log(1, "Successfully joined network:", plexus.NetworkEnvelope.Network.Name,
			"Version:", plexus.NetworkEnvelope.Network.Version,
			"Leader:", plexus.NetworkEnvelope.Network.Leader.String(),
			"Num peers:", len(plexus.NetworkEnvelope.Network.Peers),
		)
	} else {
		return *new([16]byte), errors.New("Recieved empty network version")
	}

	return authReply.UUID, nil
}

func (plexus *Plexus) addLocalPeer(newPeer *localPeer) error {
	plexus.localMapLock.Lock()
	plexus.LocalPeerMap[newPeer.UUID] = newPeer
	plexus.localMapLock.Unlock()
	return nil
}

func (plexus *Plexus) processPeeringRequest(authPacket AuthPacket) (bool, AuthPacket) {
	log(0, "Processing peering request for:", authPacket.GlobalPeer.UUID.String())
	latency := time.Since(authPacket.Time)

	if plexus.Mode == 0 {
		if err := plexus.CreateNetwork("Autogen"); err != nil {
			log(2, "Network creation failed:", err)
			return false, AuthPacket{Response: "Network creation failed"}
		}
	}

	//AddPeer
	if err := plexus.AddPeer(authPacket.GlobalPeer); err != nil {
		log(3, "Failed to add peer:", err)
		return false, AuthPacket{Response: "Failure to add"}
	}

	//Update our local peer
	newGlobalPeer := plexus.NetworkEnvelope.Network.Peers[plexus.LocalUUID]
	newGlobalPeer.ConnMap[authPacket.GlobalPeer.UUID] = latency

	if err := plexus.updatePeer(newGlobalPeer); err != nil {
		log(3, "Failed to update peer", err)
		return false, AuthPacket{Response: "Failed to update self"}
	}

	return true, AuthPacket{NetworkEnvelope: NetworkEnvelope{Network: plexus.NetworkEnvelope.Network}, Response: "ACCEPTED", UUID: plexus.LocalUUID, Time: time.Now()}
}

func (plexus *Plexus) updatePeer(peer GlobalPeer) error {
	log(0, "Requesting to update peer:", peer.UUID.String())
	if plexus.Mode == 1 {
		//I am the leader, leader command
		if err := plexus.leaderUpdatePeer(peer); err != nil {
			return err
		}
	} else {
		//I am not the leader, send a request to have the peer added
		return errors.New("Peer addition request not yet implemeneted")
	}
	return nil
}

func (plexus *Plexus) AddPeer(peer GlobalPeer) error {
	log(0, "Requesting to add peer:", peer.UUID.String())
	if plexus.Mode == 1 {
		//I am the leader, leader command
		if err := plexus.addPeerToNetwork(peer); err != nil {
			return err
		}
	} else {
		return errors.New("Peer addition request not yet implemeneted")
	}
	return nil
}

func (plexus *Plexus) createGlobalPeer() GlobalPeer {
	return GlobalPeer{UUID: plexus.LocalUUID, ConnMap: make(map[uuid.UUID]time.Duration), Version: time.Now(), PublicKey: plexus.privateKey.PublicKey, ServiceList: plexus.services}
}

func (plexus *Plexus) UpdateServices(services []string) {
	log(0, "Updating services")
	plexus.services = services
	//	plexus.updatePeer(createGlobalPeer
}
