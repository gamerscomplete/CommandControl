package plexus

import (
	"encoding/gob"
	"io"
	mathrand "math/rand"
	"net"
	"time"
)

func (plexus *Plexus) peerSyncer(SyncStream net.Conn) {
	//need to stager this time so they dont both just keep rapid firing at the same time.
	syncSeconds := mathrand.Intn(_syncMaxTime-_syncMinTime) + _syncMinTime

	syncTimer := (time.Second * time.Duration(syncSeconds))
	encoder := gob.NewEncoder(SyncStream)
	decoder := gob.NewDecoder(SyncStream)

	//this is ugly but the best ive got at the moment. needs revisiting :/
	gob.Register(&SyncPacket{})

	//need to attempt a join or create
	if plexus.Mode != 0 {
		RequestSyncPacket := SyncPacket{Version: time.Time{}, SendTime: time.Now()}
		if err := encoder.Encode(&RequestSyncPacket); err != nil {
			log(4, "Failed to encode RequestSyncPacket:", err)
		}
		var syncPacket SyncPacket
		err := decoder.Decode(&syncPacket)
		if err == io.EOF {
			//TODO: peer disconnected, handle that
			log(2, "Reached end of sync stream")
			return
		} else if err != nil {
			log(2, "syncstream decoder error:", err)
		}
	} else {
		log(3, "We started a syncer without a network. Somethin fucky happened. Bailing out. Mode:", plexus.Mode)
		return
	}

	//Listener
	var syncChan = make(chan SyncPacket)
	go func(returnChan chan SyncPacket) {
		var syncPacket SyncPacket
		for {
			//log(0, "Waiting for new sync packet")
			err := decoder.Decode(&syncPacket)
			if err == io.EOF {
				log(2, "Reached end of sync stream")
				return
			} else if err != nil {
				log(2, "syncstream decoder error:", err)
			}
			//log(0, "Recieved sync packet")
			returnChan <- syncPacket
		}
	}(syncChan)

	//Fall through switch, if we don't recieve a packet in X amount of time, then send one ourself
	for {
		var newSyncPacket SyncPacket
		select {
		case <-time.After(syncTimer):
			//log(0, "Reached timer. Sending sync packet")
			SendSyncPacket := SyncPacket{Version: plexus.NetworkEnvelope.Network.Version, SendTime: time.Now()}
			if err := encoder.Encode(&SendSyncPacket); err != nil {
				log(4, "Failed to encode syncPacket:", err)
				return
			}
		case newSyncPacket = <-syncChan:
			//!!This is relying on times being in sync. this could be problematic on nodes with bad time sync
			//latency := time.Since(newSyncPacket.SendTime)
			//			log(0, "Peer latency:", latency)
			if !newSyncPacket.Version.Equal(plexus.NetworkEnvelope.Network.Version) {
				log(1, "Map versions differs. Need to initiate sync. Local:", plexus.NetworkEnvelope.Network.Version.String(), "Remote:", newSyncPacket.Version.String())
				if newSyncPacket.Version.After(plexus.NetworkEnvelope.Network.Version) {
					//remote is newer.
					//Check if full sync packet sent. If not send our own sync packet so the peer knows we need a full sync
					if newSyncPacket.NetworkEnvelope.Signature != nil {
						//recieved a full sync packet
						if !verifySyncPacket() {
							log(2, "Verification of sync packet failed")
							continue
						}
						log(1, "Converging networks")
						plexus.updateNetwork(newSyncPacket.NetworkEnvelope)
					} else {
						//recieved update packet. send our own update packet to trigger a full sync
						SendSyncPacket := SyncPacket{Version: plexus.NetworkEnvelope.Network.Version, SendTime: time.Now()}
						if err := encoder.Encode(&SendSyncPacket); err != nil {
							log(4, "Failed to encode syncPacket:", err)
							return
						}
					}
				} else {
					//local is newer. send a full packet to the peer
					SendSyncPacket := SyncPacket{Version: plexus.NetworkEnvelope.Network.Version, SendTime: time.Now(), NetworkEnvelope: plexus.NetworkEnvelope}
					if err := encoder.Encode(&SendSyncPacket); err != nil {
						log(4, "Failed to encode full SyncPacket:", err)
					}
				}
			} else {
				//	log(0, "Map versions match. No convergance needed")
			}
		}
	}
}

func (plexus *Plexus) updateNetwork(envelope NetworkEnvelope) {
	log(1, "Updating network")
	plexus.netLock.Lock()
	//Validate the new packet
	if err := plexus.validateEnvelope(envelope); err != nil {
		log(3, "Envelope failed validation:", err)
		return
	}
	plexus.NetworkEnvelope = envelope
	plexus.generateRouteCache()
	plexus.generateServiceCache()
	defer plexus.netLock.Unlock()
}

func (plexus *Plexus) validateEnvelope(envelope NetworkEnvelope) error {
	return nil
}

//!!!THIS OUGHT TO DO SOMETHIN
func verifySyncPacket() bool {
	log(2, "verifySyncPacket() not implemented")
	return true
}
