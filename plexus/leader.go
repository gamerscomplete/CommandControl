package plexus

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/gob"
	"errors"
	"gitlab.com/gamerscomplete/uuid"
	"time"
)

func (plexus *Plexus) addPeerToNetwork(peer GlobalPeer) error {
	log(1, "Adding peer to network:", peer.UUID.String())
	if len(peer.UUID) != 16 {
		return errors.New("UUID empty")
	}
	if len(peer.ConnMap) < 1 {
		return errors.New("Cannot add a peer with no peers")
	}

	if _, found := plexus.NetworkEnvelope.Network.Peers[peer.UUID]; found {
		return errors.New("Peer already existed")
	}

	newNetwork := plexus.NetworkEnvelope.Network
	newNetwork.Peers[peer.UUID] = peer
	newNetwork.Version = time.Now()

	newEnvelope, err := plexus.buildEnvelope(newNetwork)
	if err != nil {
		return err
	}

	plexus.updateNetwork(newEnvelope)

	return nil
}

func (plexus *Plexus) leaderUpdatePeer(peer GlobalPeer) error {
	log(1, "Updating peer", peer.UUID.String())
	//make sure we actually have this guy
	if _, found := plexus.NetworkEnvelope.Network.Peers[peer.UUID]; !found {
		return errors.New("Peer does not exist")
	}
	//TODO:validate the signature matches the one already on file

	//!!!This should be functionized since its shared with adding and could double

	//create new networkenvelope
	newNetwork := plexus.NetworkEnvelope.Network
	newNetwork.Peers[peer.UUID] = peer
	newNetwork.Version = time.Now()

	newEnvelope, err := plexus.buildEnvelope(newNetwork)
	if err != nil {
		return err
	}

	//TODO:compute the network validity to ensure links are correct

	//trigger update
	plexus.updateNetwork(newEnvelope)
	return nil
}

//
func removePeerFromNetwork(uuid string) {
	//Validate the sender
	//remove from network
	log(3, "removePeerFromNetwork not implemented!")
}

func (plexus *Plexus) signNetwork(hash []byte) ([]byte, error) {
	rng := rand.Reader
	signature, err := rsa.SignPKCS1v15(rng, &plexus.privateKey, crypto.SHA256, hash[:])
	if err != nil {
		return nil, err
	}

	return signature, nil
}

func (network *Network) hash() ([]byte, error) {
	bytes, err := network.getBytes()
	if err != nil {
		return *new([]byte), err
	}

	hashed := sha256.Sum256(bytes)
	return hashed[:], nil
}

func (network *Network) getBytes() ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	if err := enc.Encode(network); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func (plexus *Plexus) validateNetwork(envelope *NetworkEnvelope) error {
	computedHash, err := envelope.Network.hash()
	if err != nil {
		return err
	}
	if !bytes.Equal(envelope.Hash, computedHash) {
		return errors.New("Hashes do not match")
	}

	value, found := plexus.NetworkEnvelope.Network.Peers[plexus.NetworkEnvelope.Network.Leader]
	if !found {
		return errors.New("Unable to retrieve the leaders peer record")
	}

	err = rsa.VerifyPKCS1v15(&value.PublicKey, crypto.SHA256, envelope.Hash, envelope.Signature)
	if err != nil {
		log(3, "Error from verification: %s\n", err)
		return err
	}
	return nil
}

func (plexus *Plexus) CreateNetwork(name string) error {
	log(1, "Creating new network:", name)

	if name == "" {
		return errors.New("Invalid name")
	}

	peerMap := make(map[uuid.UUID]GlobalPeer)
	peerMap[plexus.LocalUUID] = plexus.createGlobalPeer()

	newNetwork := Network{Name: name, Leader: plexus.LocalUUID, PrivKey: &plexus.privateKey, Status: "Active", Version: time.Now(), Peers: peerMap}

	newEnvelope, err := plexus.buildEnvelope(newNetwork)
	if err != nil {
		return err
	}

	plexus.updateNetwork(newEnvelope)

	//Set as leader
	plexus.Mode = 1

	// Should probably launch some leader services now once leader services are established lol

	return nil
}

func (plexus *Plexus) buildEnvelope(network Network) (envelope NetworkEnvelope, err error) {
	newHash, err := plexus.NetworkEnvelope.Network.hash()
	if err != nil {
		return
	}

	newSig, err := plexus.signNetwork(newHash)
	if err != nil {
		return
	}

	envelope.Hash = newHash
	envelope.Signature = newSig
	envelope.Network = network

	return
}
