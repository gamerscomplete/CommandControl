package plexus

import (
	"errors"
	"gitlab.com/gamerscomplete/uuid"
	"math"
	"time"
)

type router struct {
	startUUID     uuid.UUID
	unsolvedNodes map[uuid.UUID]node
	solvedNodes   map[uuid.UUID]node
}

//The solvednode and unsolved node should be able to share a type and will simplify moving them from one to the other
type node struct {
	Cost  int64
	Peers map[uuid.UUID]time.Duration
	Route map[int]uuid.UUID
}

func (plexus *Plexus) generateRouteCache() {
	router := router{startUUID: plexus.LocalUUID, solvedNodes: make(map[uuid.UUID]node)}
	router.unsolvedNodes = buildUnsolved(plexus.NetworkEnvelope.Network.Peers, plexus.LocalUUID)
	log(1, "Generating route cache for", len(router.unsolvedNodes), "nodes")

	for router.processNextNode() {
	}

	log(1, "Route generation complete. Generated :", len(router.solvedNodes), "routes")
	plexus.RouteMapCache = router.solvedNodes
	//complete
}

//TODO: This should go somewhere else, but since its going to be similar to routecache im putting it here
func (plexus *Plexus) generateServiceCache() {
	//[Service name][latency]node id
	serviceList := make(map[string]map[time.Duration]uuid.UUID)
	log(1, "Generating service Cache for", len(plexus.NetworkEnvelope.Network.Peers), "peers")
	for nodeID, peer := range plexus.NetworkEnvelope.Network.Peers {
		log(0, "Node:", nodeID.String(), "has", len(peer.ServiceList), "services")
		for _, serviceName := range peer.ServiceList {
			if serviceName == "" {
				log(2, "Got an empty service name. This shouldn't happen")
				continue
			}
			if _, found := serviceList[serviceName]; !found {
				//lookup time from route cache for this node
				newServiceList := make(map[time.Duration]uuid.UUID)
				newServiceList[time.Duration(plexus.RouteMapCache[nodeID].Cost)] = nodeID
				serviceList[serviceName] = newServiceList
			} else {
				serviceList[serviceName][time.Duration(plexus.RouteMapCache[nodeID].Cost)] = nodeID
			}
		}
	}
	plexus.ServiceMapCache = serviceList
}

func buildUnsolved(peers map[uuid.UUID]GlobalPeer, myid uuid.UUID) map[uuid.UUID]node {
	var unsolvedNodes = make(map[uuid.UUID]node)
	log(0, "Building unsolved map. MyID:", myid.String())
	for key, value := range peers {
		var cost int64
		var newRoute = make(map[int]uuid.UUID)
		if key == myid {
			cost = 0
			newRoute[0] = myid
		} else {
			cost = math.MaxInt64
		}
		log(0, "Adding to unsolvedNodes", key.String(), "Peer length:", len(value.ConnMap))
		unsolvedNodes[key] = node{Cost: cost, Peers: value.ConnMap, Route: newRoute}
	}
	return unsolvedNodes
}

func (router *router) processNextNode() bool {
	log(0, "Processing next node")
	if len(router.unsolvedNodes) == 0 {
		return false
	}
	var lowestCost int64
	lowestCost = math.MaxInt64
	var lowestNode uuid.UUID
	for key, value := range router.unsolvedNodes {
		if value.Cost < lowestCost {
			lowestCost = value.Cost
			lowestNode = key
		}
	}

	if lowestCost == math.MaxInt64 {
		log(2, "Failed to find next peer")
		return false
	}

	router.solve(lowestNode)
	return true
}

func (router *router) solve(toSolve uuid.UUID) {
	log(0, "Solving for:", toSolve.String())
	if len(router.unsolvedNodes) < 1 {
		log(2, "Unsolved nodes empty")
	}
	log(0, "Iterating loop of", len(router.unsolvedNodes[toSolve].Peers))
	//loop through all local peers, add their costs to our nodes cost.
	for key, value := range router.unsolvedNodes[toSolve].Peers {
		log(0, "Processing peer:", key.String())
		newCost := int64(value) + router.unsolvedNodes[toSolve].Cost
		if newCost < router.unsolvedNodes[key].Cost {
			log(0, "Found shorter route for", key.String(), "With a cost of:", newCost)
			//replace the route with the new route
			var newRoute = make(map[int]uuid.UUID)
			for k, v := range router.unsolvedNodes[toSolve].Route {
				newRoute[k] = v
			}
			newRoute[len(router.unsolvedNodes[toSolve].Route)] = key
			newNode := node{Cost: newCost, Peers: router.unsolvedNodes[key].Peers, Route: newRoute}
			log(0, "Assigning newNode to unsolved with a length of:", len(newRoute))
			router.unsolvedNodes[key] = newNode
		} else {
			log(0, "Longer route for ", key.String(), "their weight:", router.unsolvedNodes[key].Cost, "total new weight", newCost)
		}
	}
	log(0, "Adding node to solved nodes", toSolve.String())
	router.solvedNodes[toSolve] = router.unsolvedNodes[toSolve]
	log(0, "Removing", toSolve.String(), "from unsolvednodes")
	delete(router.unsolvedNodes, toSolve)
	log(0, "unsolved nodes remaining:", len(router.unsolvedNodes))
	//Once complete, the node can be removed from the unsolvedNodes and added to the solvedNodes list
}

func (plexus *Plexus) getRouteHop(destination uuid.UUID) (uuid.UUID, error) {
	if _, found := plexus.RouteMapCache[destination]; !found {
		log(3, "Destination does not have a route cache")
		return uuid.UUID{}, errors.New("Destination doe snot have a route cache")
	}
	if _, found := plexus.RouteMapCache[destination].Route[1]; !found {
		//		log(3, "Attempted to get route for invalid node")
		log(0, "route length", len(plexus.RouteMapCache[destination].Route))
		return uuid.UUID{}, errors.New("Route does not exist for destination")
	}
	return plexus.RouteMapCache[destination].Route[1], nil
}
