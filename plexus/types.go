package plexus

import (
	"crypto/rsa"
	"github.com/hashicorp/yamux"
	"gitlab.com/gamerscomplete/CommandControl/clientServerTools"
	"gitlab.com/gamerscomplete/uuid"
	"net"
	"sync"
	"time"
)

////////////////////////////////////////////////////////////
/// Types
////////////////////////////////////////////////////////////
type fn func(clientServerTools.ServerMessage, net.Conn)

type Plexus struct {
	LocalPeerMap    map[uuid.UUID]*localPeer
	localMapLock    sync.Mutex
	LocalUUID       uuid.UUID
	callback        fn
	privateKey      rsa.PrivateKey
	netLock         sync.Mutex
	NetworkEnvelope NetworkEnvelope
	//Mode designates if this is leader or follower
	Mode          int
	RouteMapCache map[uuid.UUID]node
	//[service name]map[start time]node id
	ServiceMapCache map[string]map[time.Duration]uuid.UUID
	services        []string
}

type GlobalPeer struct {
	UUID        uuid.UUID
	ConnMap     map[uuid.UUID]time.Duration
	ServiceList []string
	JoinTime    time.Time
	Version     time.Time
	PublicKey   rsa.PublicKey
}

type SyncLog struct {
	TimeStamp time.Time
	Type      int
	Message   string
}

type SyncPacket struct {
	// 0 - Version check
	// 1 - Join request
	// 2 - Update
	Type            int
	SendTime        time.Time
	Version         time.Time
	NetworkEnvelope NetworkEnvelope
}

type AuthPacket struct {
	//All
	Time time.Time
	UUID uuid.UUID
	//Requests
	GlobalPeer GlobalPeer
	PubKey     rsa.PublicKey
	//Replies
	NetworkEnvelope NetworkEnvelope
	Response        string
}

type stream struct {
	name         string
	sentSize     int
	recievedSize int
	steam        yamux.Stream
}

type PeerMessage struct {
	To          uuid.UUID
	From        uuid.UUID
	MessageType int
	Payload     interface{}
}

//currently only being used for leader messages
type InternalMessage struct {
	// 1 - Request peer addition
	// 2 - Update peer
	// 3 - Vote?
	ID         int64
	Type       int
	GlobalPeer GlobalPeer
	Approved   bool
}

type Network struct {
	Name    string
	Version time.Time
	PrivKey *rsa.PrivateKey
	//This is subpar but easy for now and makes the code more readable by using strings instead of ints
	Status string
	Ledger map[int]string
	Leader uuid.UUID
	Peers  map[uuid.UUID]GlobalPeer
}

type NetworkEnvelope struct {
	Network   Network
	Hash      []byte
	Signature []byte
}

type localPeer struct {
	UUID             uuid.UUID
	Address          string
	Port             int
	SentSize         int
	RecievedSize     int
	MessagesSent     int
	MessagesRecieved int
	Streams          map[string]stream
	StreamSession    yamux.Session
	latency          time.Time
	SendQueue        chan PeerMessage
}
