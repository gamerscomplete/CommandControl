// BORN TO RAGE

//Known bugs:
//unpacktype isn't failing when it recieves an incorrect type

package clientServerTools

import (
	"bufio"
	"bytes"
	"encoding/gob"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"strconv"
	"time"
	"unsafe"

	"github.com/hashicorp/yamux"
	"gitlab.com/gamerscomplete/CommandControl/tools"
	"gitlab.com/gamerscomplete/uuid"
)

const (
	_CCSockLocation    = "/tmp/CommandControl/cc.stream.sock"
	logLevel           = 2
	TIMEOUT            = time.Second * time.Duration(1)
	RECIEVE_QUEUE_SIZE = 3
)

var (
	shutdownChan = make(chan string)
	_serviceName string
)

func NewServerConn() *ServerConnObj {
	newConn := ServerConnObj{handlers: make(map[string]handlerFunc)}
	newConn.RecieveQueue = make(chan ServerMessage, RECIEVE_QUEUE_SIZE)
	newConn.SendQueue = make(chan Job)
	newConn.JobIDChannel = make(chan int64, 3)
	newConn.ID = GetID()
	go newConn.jobManager()
	go newConn.jobIDGenerator()
	return &newConn
}

func (serverConn *ServerConnObj) Init(name string) error {
	serverConn.ServiceName = name
	_serviceName = name

	authToken := getAuthToken()
	if err := serverConn.establishControllerConnection(authToken); err != nil {
		return err
	}
	return nil
}

/*
func (serverConn *ServerConnObj) NewStream() (net.Conn, error) {
	log(0, "Creating new stream")
	stream, err := serverConn.StreamSession.Open()
	if err != nil {
		log(4, err)
		return err, nil
	}
	return nil, stream
}
*/

// Establish connection to the server
func (serverConn *ServerConnObj) establishControllerConnection(authToken string) (err error) {
	if serverConn.Conn != nil {
		return errors.New("ServerConn is already established")
	}
	if serverConn.ControlStream != nil {
		return errors.New("ControlStream is already established")
	}

	serverConn.Conn, err = net.Dial("unix", _CCSockLocation)
	if err != nil {
		return err
	}

	log(0, "Creating client session")
	serverConn.StreamSession, err = yamux.Client(serverConn.Conn, nil)
	if err != nil {
		log(4, err)
		return err

	}
	log(0, "Creating control stream")
	serverConn.ControlStream, err = serverConn.StreamSession.Open()
	if err != nil {
		log(4, err)
		return err
	}

	decoder := json.NewDecoder(bufio.NewReader(serverConn.ControlStream))

	//Need to establish authentication

	go func(decoder json.Decoder) {
		for {
			var serverMessage ServerMessage
			if err := decoder.Decode(&serverMessage); err == io.EOF {
				log(5, "Server hung up on us")
				break
			} else if err != nil {
				log(4, "Server error. Disconecting: ", err)
				serverConn.Conn = nil
				//TODO: Need to probably close streams. Need tof ind out if they autoclose on underlying connection closure
				serverConn.StreamSession = nil
				break
			}

			serverConn.RecievedSize += int64(unsafe.Sizeof(serverMessage))

			if serverMessage.IsReply == true {
				serverConn.RecieveQueue <- serverMessage
			} else {
				go serverConn.handleMessage(serverMessage)
			}
		}
	}(*decoder)

	authPacket := Message{Command: "AUTH", Parameters: authToken}
	if returnChan, err := serverConn.sendServerMessageTwoWay(serverConn.ID, "genesis", authPacket); err == nil {
		response := <-returnChan
		if response.Parameters != "SUCCESS" {
			log(0, "Authentication failed: ", response.Parameters)
		} else {
			log(0, "Authentication with genesis successful")
		}
	} else {
		log(0, "Failed to send auth packet")
	}
	return nil
}

func (serverConn *ServerConnObj) handleMessage(message ServerMessage) {
	message.serverConn = serverConn
	serverConn.handlerLock.RLock()
	handler, found := serverConn.handlers[message.Data.Command]
	serverConn.handlerLock.RUnlock()
	if !found {
		serverConn.ReplyError(message, "Unknown command recieved: "+message.Data.Command)
		return
	}
	if err := handler(message); err != nil {
		replyMessage := Message{Command: "REPLY"}
		replyMessage.Error = err.Error()
		serverConn.SendServerMessageReply(message, replyMessage)
	}
}

func log(level int, message ...interface{}) {
	if level < logLevel {
		return
	}
	fmt.Print("(clientServerTools:"+_serviceName+")", message, "\n")
}

// Retrieve authentication token that was set as an environment variable when genesis launched us
func getAuthToken() string {
	authToken := os.Getenv("AUTH_TOKEN")
	if authToken == "" {
		log(5, "Auth token not found in environment variable. Exiting")
		os.Exit(1)
	}
	return authToken
}

//just need this to work right now. turn into environment parser later
func GetID() uuid.UUID {
	id := os.Getenv("ID")
	if id == "" {
		log(5, "ID not found in environment variable. Exiting")
		os.Exit(1)
	}

	newID := new(uuid.UUID)
	copy(newID[:], id)
	return *newID
}

// Marshal the message and send it to the server
func (serverConn *ServerConnObj) packAndSend(serverMessage ServerMessage) (err error) {
	//	fmt.Println("Sending message")
	marshaledMessage, err := json.Marshal(serverMessage)
	if err != nil {
		return err
	}
	//I think this is the size we are looking for but will check it later. do something with the _!
	if serverConn.ControlStream == nil {
		return errors.New("ControlStream is dead wtf")
	}
	sentSize, err := serverConn.ControlStream.Write([]byte(marshaledMessage))
	if err != nil {
		return err
	}
	serverConn.SentSize += int64(sentSize)
	return nil
}

// Send message without a return job
func (serverConn *ServerConnObj) SendServerMessage(destinationID uuid.UUID, destinationService string, message Message) error {
	var serverMessage = ServerMessage{
		ToID:        destinationID,
		ToService:   destinationService,
		FromService: serverConn.ServiceName,
		JobID:       0,
		IsReply:     false,
		Data:        message,
		Time:        tools.EpochTime(),
	}
	return serverConn.packAndSend(serverMessage)
}

func (serverMessage *ServerMessage) Reply(message Message) error {
	var newServerMessage = ServerMessage{
		ToID:        serverMessage.FromID,
		ToService:   serverMessage.FromService,
		FromService: serverMessage.serverConn.ServiceName,
		JobID:       serverMessage.JobID,
		IsReply:     true,
		Data:        message,
		Time:        tools.EpochTime(),
	}
	return serverMessage.serverConn.packAndSend(newServerMessage)
}

func (serverConn *ServerConnObj) SendServerMessageReply(message ServerMessage, replyMessage Message) error {
	var serverMessage = ServerMessage{
		ToID:        message.FromID,
		ToService:   message.FromService,
		FromService: serverConn.ServiceName,
		JobID:       message.JobID,
		IsReply:     true,
		Data:        replyMessage,
		Time:        tools.EpochTime(),
	}
	return serverConn.packAndSend(serverMessage)
}

func (serverConn *ServerConnObj) ReplyError(serverMessage ServerMessage, message string) error {
	replyMessage := Message{Command: "REPLY", Error: message}
	return serverConn.SendServerMessageReply(serverMessage, replyMessage)
}

// Send message with a return
func (serverConn *ServerConnObj) sendServerMessageTwoWay(destinationID uuid.UUID, destinationService string, message Message) (returnChannel chan Message, err error) {
	jobChan := make(chan Message)
	jobID := <-serverConn.JobIDChannel
	log(0, "Job ID: ", jobID)
	job := Job{ID: jobID, Channel: jobChan}

	serverConn.SendQueue <- job
	serverMessage := ServerMessage{
		ToID:        destinationID,
		ToService:   destinationService,
		FromService: serverConn.ServiceName,
		JobID:       job.ID,
		IsReply:     false,
		Data:        message,
		Time:        tools.EpochTime(),
	}

	err = serverConn.packAndSend(serverMessage)
	return jobChan, err
}

// Handles two way message sending
func (serverConn *ServerConnObj) jobManager() {
	var jobMap = make(map[int64]chan Message)
	for {
		select {
		case send := <-serverConn.SendQueue:
			jobMap[send.ID] = send.Channel
		case serverMessage := <-serverConn.RecieveQueue:
			//Check to make sure the element is in the map
			_, exists := jobMap[serverMessage.JobID]
			if exists == false {
				//didnt exist need to find an appropriate way to handle this.
				log(3, "Recieved reply to job not in queue from: "+serverMessage.FromID.String()+":"+serverMessage.FromService+" ID:", serverMessage.JobID, serverMessage)
				log(0, "Queue length:", len(jobMap))
				break
			}
			// This is going to jam us up on parallel jobs, need a way to goroutine this and delete the job from the map
			go func(serverMessage ServerMessage, jobChannel chan Message) {
				jobChannel <- serverMessage.Data
			}(serverMessage, jobMap[serverMessage.JobID])
			delete(jobMap, serverMessage.JobID)
		}
	}
}

// Generate ID's for jobs.
// Likely will be deprecated quickly
func (serverConn *ServerConnObj) jobIDGenerator() {
	//	log(0, "Starting job generator")
	var jobID int64
	for {
		serverConn.JobIDChannel <- jobID
		jobID++
	}
}

func (serverConn *ServerConnObj) LogToServer(level int, message string) (err error) {
	var logMessage = Message{Command: "LOG"}
	err = serverConn.SendServerMessage(serverConn.ID, "genesis", logMessage)
	return err
}

func (message *Message) PackType(input interface{}) error {
	packaged := new(bytes.Buffer)
	encoder := gob.NewEncoder(packaged)
	if err := encoder.Encode(input); err != nil {
		return err
	}
	message.Payload = packaged.Bytes()
	return nil
}

func (serverConn *ServerConnObj) PackSend(command string, serviceName string, destinationID uuid.UUID, unpackedType interface{}) error {
	message := Message{Command: command}
	if err := message.PackType(unpackedType); err != nil {
		return err
	}

	serverMessage := ServerMessage{
		ToID:        destinationID,
		ToService:   serviceName,
		FromService: serverConn.ServiceName,
		IsReply:     false,
		Data:        message,
		Time:        tools.EpochTime(),
	}

	return serverConn.packAndSend(serverMessage)
}

func (serverConn *ServerConnObj) PackSendReplyTimeout(timeout int, command string, serviceName string, nodeID uuid.UUID, sendMessage interface{}, unpackedMessage interface{}) error {
	requestMessage := Message{Command: command}

	if sendMessage != nil {
		if err := requestMessage.PackType(sendMessage); err != nil {
			return err
		}
	}

	replyMessage, err := serverConn.TimedQuery(time.Second*time.Duration(timeout), requestMessage, nodeID, serviceName)
	if err != nil {
		return err
	}
	if replyMessage.Error != "" {
		return errors.New(replyMessage.Error)
	}

	if unpackedMessage == nil {
		return nil
	}

	//Tried to use unpack but had issues passing the interfaces through
	decoder := gob.NewDecoder(bytes.NewBuffer(replyMessage.Payload))
	return decoder.Decode(unpackedMessage)
}

//TODO: Errors not actually surfacing correctly
func (message *Message) UnpackType(output interface{}) error {
	decoder := gob.NewDecoder(bytes.NewBuffer(message.Payload))
	return decoder.Decode(output)
}

func registerTypes() {
	gob.Register(&ClusterServiceMap{})
}

func (serverConn *ServerConnObj) Ping(addr uuid.UUID) (latency int, err error) {
	startTime := time.Now()
	_, err = serverConn.TimedQuery(TIMEOUT, Message{Command: "PING"}, addr, "genesis")
	if err != nil {
		fmt.Println("timed query failed")
		return
	}
	return int(time.Since(startTime).Milliseconds()), nil
}

func (serverConn *ServerConnObj) ConnectTo(addr string, port int) (peerID uuid.UUID, err error) {
	if addr == "" {
		return peerID, errors.New("address not provided")
	}
	if port == 0 || port > 65535 {
		return peerID, errors.New("invalid port")
	}

	connectString := addr + ":" + strconv.Itoa(port)
	returnMessage, err := serverConn.TimedQuery(TIMEOUT, Message{Command: "PLEXUS_CONNECT", Parameters: connectString}, serverConn.ID, "genesis")
	if err != nil {
		fmt.Println("timed query failed")
		return peerID, err
	}

	err = returnMessage.UnpackType(&peerID)
	return peerID, err
}

func (serverConn *ServerConnObj) StartService(nodeID uuid.UUID, serviceName string) error {
	if err := serverConn.PackSendReplyTimeout(5, "START_SERVICE", "genesis", nodeID, serviceName, nil); err != nil {
		return err
	}
	return nil
}

func (serverConn *ServerConnObj) StopService(serviceID uuid.UUID, serviceName string) error {
	if err := serverConn.PackSendReplyTimeout(5, "STOP_SERVICE", "genesis", serviceID, serviceName, nil); err != nil {
		return err
	}
	return nil
}

//Filter does fuckall
func (serverConn *ServerConnObj) GetClusterServices(filter string) (returnMap ClusterServiceMap, err error) {
	returnMessage, err := serverConn.TimedQuery(TIMEOUT, Message{Command: "GET_SERVICES"}, serverConn.ID, "genesis")
	if err != nil {
		return returnMap, err
	}

	err = returnMessage.UnpackType(&returnMap)
	return
}

func (serverConn *ServerConnObj) GetServiceNodesByName(name string) ([]uuid.UUID, error) {
	var results []uuid.UUID
	if err := serverConn.PackSendReplyTimeout(5, "SERVICE_NODES_BY_NAME", "genesis", serverConn.ID, name, &results); err != nil {
		return nil, err
	}
	return results, nil
}

func (serverConn *ServerConnObj) TimedQuery(timer time.Duration, message Message, destinationID uuid.UUID, destinationService string) (returnMessage Message, err error) {
	returnChannel, err := serverConn.sendServerMessageTwoWay(destinationID, destinationService, message)
	if err != nil {
		log(3, "Failed to send message", err)
		return returnMessage, err
	}

	select {
	case replyMessage := <-returnChannel:
		return replyMessage, nil
	case <-time.After(timer):
		return returnMessage, errors.New("Timed out")
	}
}

func Wait() {
	_ = <-shutdownChan
}

func (serverConn *ServerConnObj) RegisterHandler(command string, handler handlerFunc) {
	serverConn.handlerLock.Lock()
	serverConn.handlers[command] = handler
	serverConn.handlerLock.Unlock()
}
