package clientServerTools

import (
	"net"
	"sync"
	"time"

	"github.com/hashicorp/yamux"
	"gitlab.com/gamerscomplete/uuid"
)

type handlerFunc func(ServerMessage) error

type QueueMessage struct {
	Timestamp int64
	JobID     int64
	Message   ServerMessage
}

type ServerConnObj struct {
	Conn             net.Conn
	StreamSession    *yamux.Session
	ControlStream    net.Conn
	SentSize         int64
	RecievedSize     int64
	MessagesSent     int64
	MessagesRecieved int64
	MessagesFailed   int64
	RecieveQueue     chan ServerMessage
	SendQueue        chan Job
	JobIDChannel     chan int64
	ServiceName      string
	ID               uuid.UUID
	handlers         map[string]handlerFunc
	handlerLock      sync.RWMutex
}

type Message struct {
	Command    string
	Parameters string
	Signature  []byte
	Payload    []byte
	Error      string
}

type ServerMessage struct {
	ToID        uuid.UUID
	FromID      uuid.UUID
	ToService   string
	FromService string
	JobID       int64
	IsReply     bool
	Data        Message
	Time        int64
	serverConn  *ServerConnObj
}

type Job struct {
	ID      int64
	Channel chan Message
}

///Genesis return types

type ClusterServiceMap struct {
	//[node id][service name]latency
	Services map[string]map[time.Duration]uuid.UUID
}
